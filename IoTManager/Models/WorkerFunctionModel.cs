﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    //public class DeviceGroupModel
    //{
    //    public long? Id { get; set; }
    //    public string Name { get; set; }
    //}

    public class WorkerFunctionModel
    {
        public long TaskId { get; set; }
        public bool TaskActive { get; set; }
        public long DeviceId { get; set; }
        public long FunctionId { get; set; }
        public string FunctionDescription { get; set; }
        public string VersionDescription { get; set; }
        public string VersionCode { get; set; }        
    }    
}
