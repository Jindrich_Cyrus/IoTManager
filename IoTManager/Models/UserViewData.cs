﻿using Newtonsoft.Json;
using NLog;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    public class UserViewData
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string Name;
        public Dictionary<string, bool> Rights = new Dictionary<string, bool>();
        public Dictionary<string, bool> Groups = new Dictionary<string, bool>();
        public Dictionary<long, string> AllGroups = new Dictionary<long, string>();
        public Dictionary<long, string> AllVersionStates = new Dictionary<long, string>();

        public static UserViewData FromDatabase(long userId, string connectionString)
        {
            var result = new UserViewData();
            try
            {
                using (var connection = new NpgsqlConnection(connectionString))
                {
                    connection.Open();

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT name FROM users WHERE user_id = @user_id", connection);
                        command.Parameters.AddWithValue("@user_id", userId);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                result.Name = reader["name"] as string;
                            }
                        }
                    }

                    //var groups = new List<DeviceGroupModel>();

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT rights.name FROM user_right, rights WHERE user_right.rights_id = rights.right_id AND user_right.user_id = @user_id", connection);
                        command.Parameters.AddWithValue("@user_id", userId);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Rights[reader["name"] as string] = true;
                            }
                        }
                    }

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT group_id, name FROM device_groups", connection);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.AllGroups.Add(Convert.ToInt64(reader["group_id"]), reader["name"] as string);
                            }
                        }
                    }

                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT version_state_id, name FROM version_state", connection);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.AllVersionStates.Add(Convert.ToInt64(reader["version_state_id"]), reader["name"] as string);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failed to read user");
            }

            return result;
        }
    }
}
