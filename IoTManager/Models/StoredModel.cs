﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    public class StoredModel
    {
        public bool Stored { get; set; }

        public StoredModel()
        {
            Stored = false;
        }
    }

}
