﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoTManager.Models
{
    public class HostedFunction
    {
        public DeviceModel Device { get; set; }
        public FunctionModel Function { get; set; }
    }

    public class TaskModel: StoredModel
    {
        public string Error { get; set; }
        public long Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }

        public Dictionary<int, HostedFunction> Hosts { get; set; }
    }
}
