﻿var React = require('react');
var createReactClass = require('create-react-class');
var AggregatedCounts = require('./components/aggregatedCounts');
var DeviceRow = require('./components/deviceRow');
import { hasRight } from './components/userRights'
import { Button, Table, Input, Dropdown, Label } from 'semantic-ui-react';
import { Map } from 'immutable';
import { tr } from './components/translation';

var DevicesList = createReactClass({

    getInitialState: function () {
        return { devices: [], checked: Map({}), counts: { online: 0, offline: 0, updates: 0 }, filter: Map({ online: true, offline: true, updates: true, text: '', lines: 5 }), timeoutId: null };
    },

    getPollInterval() {
        return this.props.pollInterval ? this.props.pollInterval : 2000;
    },

    componentDidMount: function () {
        this.loadDevices();
    },

    componentWillUnmount: function () {
        if (this.state.timeoutId !== null) clearTimeout(this.state.timeoutId);
    },

    checkedChangedHandler: function (id) {
        let value = this.state.checked.get(id);
        this.setState({ checked: this.state.checked.set(id, !value) });
    },

    updateDevices(devices) {
        if (devices && Array.isArray(devices)) {
            this.setState({ devices: devices });
            var counts = { online: 0, offline: 0, updates: 0 };
            devices.forEach((device) => {
                if (device.online) counts.online++;
                if (!device.online) counts.offline++;
                if (device.updates) counts.updates++;
            });
            this.setState({ counts: counts });
        }
    },

    loadDevices: function () {
        $.ajax({
            url: "device.list",
            cache: false,
            context: this,
            data: {}
        })
            .done(function (devices) {
                this.updateDevices(devices);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
                this.setState({ timeoutId: setTimeout(this.loadDevices, this.getPollInterval()) });
            });
    },

    deleteSelected: function () {
        this.state.checked.map((value, index) => {
            if (value) this.deleteDevice(index);
            this.setState({state: this.state.checked.delete(index)});
        })
    },

    deleteDevice: function (id) {
        $.ajax({
            url: "device.delete",
            cache: false,
            context: this,
            data: {id: id}
        })
            .done(function (devices) {
                this.updateDevices(devices);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    addDevice: function () {
        $.ajax({
            url: "device.add",
            cache: false,
            context: this,
            data: { }
        })
            .done(function (devices) {
                this.updateDevices(devices);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    toggleShowAll() {
        let filter = this.state.filter;
        filter = filter.set('lines', (filter.get('lines') < 0) ? 5 : -1);
        this.setState({ filter: filter });
    },

    filterOnline() {
        var filter = this.state.filter;
        filter = filter.set('online', true);
        filter = filter.set('offline', false);
        filter = filter.set('updates', false);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterOffline() {
        var filter = this.state.filter;
        filter = filter.set('online', false);
        filter = filter.set('offline', true);
        filter = filter.set('updates', false);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterUpdates() {
        var filter = this.state.filter;
        filter = filter.set('online', false);
        filter = filter.set('offline', false);
        filter = filter.set('updates', true);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterReset() {
        var filter = this.state.filter;
        filter = filter.set('online', true);
        filter = filter.set('offline', true);
        filter = filter.set('updates', true);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterByText(event, { value }) {
        var filter = this.state.filter;
        filter = filter.set('online', true);
        filter = filter.set('offline', true);
        filter = filter.set('updates', true);
        filter = filter.set('text', value);
        this.setState({ filter: filter });
    },

    render: function () {

        var lines = 0;
        var rows = this.state.devices.filter(function (device) {
            if (!((device.online && this.state.filter.get('online')) ||
                (!device.online && this.state.filter.get('offline')) ||
                (device.updates && this.state.filter.get('updates')))) {
                return false;
            }

            if (this.state.filter.get('text')) {
                if (!device.description && !device.groupName) return false;
                if (!(device.description && device.description.toLowerCase().search(this.state.filter.get('text')) != -1) &&
                    !(device.groupName && device.groupName.toLowerCase().search(this.state.filter.get('text')) != -1)) {
                    return false;
                }
            }

            lines++;
            return (this.state.filter.get('lines') >= 0 && lines > this.state.filter.get('lines')) ? false : true;

        }, this)
            .map((device, index) =>
                <DeviceRow key={device.id} device={device} checked={(this.state.checked.get(device.id)) ? true : false} checkedChanged={this.checkedChangedHandler} />
            );

        return (
            <div>
                <header className="main-header">
                    <div className="main-column">
                        <div className="left-header">
                            <h1>{tr("Devices list")}</h1>
                            <div className="menu-buttons green">
                                <Dropdown icon={null} trigger={<span><Input icon='search' placeholder={tr("Filter")} onChange={this.filterByText} /></span>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.filterOnline}><Label circular color="green" empty />{tr("Online")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterOffline}><Label circular color="red" empty />{tr("Offline")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterUpdates}><Label circular color="yellow" empty />{tr("Updates")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterReset}>{tr("Reset filter")}</Dropdown.Item>
                                        {hasRight('administrator') && <Dropdown.Item onClick={this.deleteSelected}>{tr("Delete selected device")}</Dropdown.Item>}
                                        {hasRight('administrator') && <Dropdown.Item onClick={this.addDevice}>{tr("Add device")}</Dropdown.Item>}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                        <div className="header-aggregated-values">
                            <AggregatedCounts total={this.state.counts.online + this.state.counts.offline} count1={this.state.counts.online} count2={this.state.counts.offline} count3={this.state.counts.updates} />
                        </div>
                    </div>
                </header>
                <header className="sub-header main-column">
                    <div className="header-aggregated-values">
                        <span className="box">{tr("Device")}</span>
                        <div className="detail-values">
                            <span className="box">{tr("Online")}</span>
                            <span className="box">{tr("Offline")}</span>
                            <span className="box">{tr("Updates")}</span>
                        </div>
                    </div>
                </header>
                <div className="container body-content main-column">
                    <Table striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell></Table.HeaderCell>
                                <Table.HeaderCell>{tr("Description")}</Table.HeaderCell>
                                <Table.HeaderCell>{tr("Group")}</Table.HeaderCell>
                                <Table.HeaderCell width="1"></Table.HeaderCell>
                                <Table.HeaderCell>{tr("Status")}</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        {rows}
                    </Table>
                    <div className="footer">
                        <Button onClick={this.toggleShowAll}>
                            {(this.state.filter.get('lines') < 0) ? tr("Show") + " 5" : tr("Show all")}
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = DevicesList;
