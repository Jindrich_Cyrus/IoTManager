﻿var React = require('react');
var createReactClass = require('create-react-class');
var FunctionVariableSetter = require('./components/functionVariableSetter');
var TaskDeviceSelector = require('./components/taskDeviceSelector');
var TaskFunctionSelector = require('./components/taskFunctionSelector');
var InputLink = require('./components/inputLink');
import { Card, Modal, Icon, Menu, Grid, Segment, TextArea, Select, Input, Form, Button, Table, Message, Checkbox } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { isEqualVariableTypes, getName } from './components/utils'
import { tr } from './components/translation';

var TaskDetail = createReactClass({

    maxLinkColors: 8,

    getInitialState: function () {
        return {
            task: {},
            dialog: { components: null, header: '', componentType: null, editedHost: null },
            changed: false,
            selectedInput: null,
            selectedOutput: null,
            linkRefresherIntervalId: null,
            error: null
        };
    },

    componentDidMount: function () {
        window.addEventListener("resize", this.refreshLinks);
        this.setState({ linkRefresherIntervalId: setInterval(this.refreshLinks, 500) });
        this.loadTask();
    },

    componentWillUnmount: function () {
        if (this.state.linkRefresherIntervalId !== null) {
            clearInterval(this.state.linkRefresherIntervalId);
            this.setState({ linkRefresherIntervalId: null });
        }
    },

    showComponents: function (editedHost, type) {
        var header = '';
        switch (type) {
            case "producers":
                header = tr("Select function");
                this.loadFunctions(type);
                break;
            case "consumers":
                header = tr("Select function");
                this.loadFunctions(type);
                break;
            case "processors":
                header = tr("Select function");
                this.loadFunctions(type);
                break;
            case "devices":
                header = tr("Select device for function") + ' ' + getName(editedHost.function.description);
                this.loadDevices();
                break;
        }
        var dialog = this.state.dialog;
        dialog.header = header;
        dialog.components = null;
        dialog.editedHost = editedHost ? editedHost : this.state.dialog.editedHost
        dialog.componentType = type;

        this.setState({ dialog: dialog })
    },

    updateTask(task) {
        if (task) {
            if (task.error) {
                this.setState({ error: task.error });
                return false;
            }
            else {
                if (!task.hosts) task.hosts = {};
                else {
                    for (var hostKey in task.hosts) {
                        task.hosts[hostKey].key = hostKey;
                        // update data sources
                        if (task.hosts[hostKey].function && task.hosts[hostKey].function.inputs) {
                            var inputs = task.hosts[hostKey].function.inputs;
                            for (var inputKey in task.hosts[hostKey].function.inputs) {
                                if (inputs[inputKey].dataSource) {
                                    inputs[inputKey].dataSource = this.populateDataSource(inputs[inputKey].dataSource, task.hosts);
                                }
                            }
                        }
                    }
                }
                this.setState({ task: task, changed: false });
                this.refreshLinks();
                return true;
            }
        }
    },

    loadTask() {
        if (!this.props.match || this.props.match.params.id === undefined || this.props.match.params.id == 'new') {
            this.updateTask({});
        } else {
            $.ajax({
                url: "task.detail",
                cache: false,
                context: this,
                data: { id: this.props.match.params.id }
            })
                .done(function (task) {
                    this.updateTask(task);
                })
                .fail(function () {
                    if (jqXHR.status == 401) window.location = "home.login";
                })
                .always(function () {
                });
        }
    },

    updateDevices(devices) {
        if (devices && Array.isArray(devices)) {
            var dialog = this.state.dialog;
            dialog.components = <TaskDeviceSelector devices={devices} device={this.state.dialog.editedHost && this.state.dialog.editedHost.device} onSelected = { this.assignDevice } />
            this.setState({ dialog: dialog });
        }
    },

    loadDevices: function () {
        $.ajax({
            url: "device.list",
            cache: false,
            context: this,
            data: {}
        })
            .done(function (devices) {
                this.updateDevices(devices);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    hasProperties(object) {
        if (!object) return false;
        return Object.keys(object).length > 0;
    },

    updateFunctions(functions) {
        if (functions && Array.isArray(functions)) {
            var dialog = this.state.dialog;
            var correctFunctionsType = functions.filter((fce) => {
                switch (dialog.componentType) {
                    case "producers":
                        if (!this.hasProperties(fce.outputs)) return false;
                        if (this.hasProperties(fce.inputs)) return false;
                        return true;
                    case "consumers":
                        if (!this.hasProperties(fce.inputs)) return false;
                        if (this.hasProperties(fce.outputs)) return false;
                        return true;
                    case "processors":
                        if (!this.hasProperties(fce.outputs)) return false;
                        if (!this.hasProperties(fce.inputs)) return false;
                        return true;
                    default: return false;
                }
            }, this);
            dialog.components = <TaskFunctionSelector functions={correctFunctionsType} function={this.state.dialog.editedHost && this.state.dialog.editedHost.function} onSelected={this.assignFunction} />

            this.setState({ dialog: dialog });
        }
    },

    loadFunctions: function (type) {
        $.ajax({
            url: "function.list",
            cache: false,
            context: this,
            data: {}
        })
            .done(function (functions) {
                this.updateFunctions(functions);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    saveTask() {
        $.ajax({
            method: 'POST',
            url: "task.edit",
            cache: false,
            context: this,
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(this.state.task)
        })
            .done(function (task) {
                if (this.updateTask(task)) {
                    var link = document.body.querySelector("h2.backlink a");
                    if (link) {
                        link.click();
                    }
                }
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    copyTask() {
        var newTask = JSON.parse(JSON.stringify(this.state.task));
        newTask.name = tr("copy") + " " + newTask.name;
        newTask.description = newTask.description;
        newTask.stored = false;
        newTask.active = false;

        $.ajax({
            method: 'POST',
            url: "task.edit",
            cache: false,
            context: this,
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(newTask)
        })
            .done(function (task) {
                var a = document.createElement("a");
                a.setAttribute("href", "#/task/" + task.id);
                a.click();
                this.updateTask(task);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    changeTaskActive(event, { value }) {
        this.state.task.active = !this.state.task.active;
        this.setState({ task: this.state.task, changed: true });
    },

    changeTaskName(event, { value }) {
        this.state.task.name = value;
        this.setState({ task: this.state.task, changed: true });
    },

    changeTaskDescription(event, { value }) {
        this.state.task.description = value;
        this.setState({ task: this.state.task, changed: true });
    },

    assignDevice(device) {
        if (this.state.dialog.editedHost) {
            this.state.dialog.editedHost.device = device;
            if (!this.state.dialog.editedHost.key) this.addHost(this.state.dialog.editedHost);
            this.setState({ task: this.state.task, changed: true });
        }
        this.hideComponents();
    },

    removeHost(host) {
        this.removeConnectedLinks(this.state.task.hosts[host.key]);
        delete this.state.task.hosts[host.key];
        this.setState({ task: this.state.task, changed: true });
        this.refreshLinks();
    },

    addHost(host) {
        if (!host.key) {
            var max = 0;
            for (var key in this.state.task.hosts) {
                var intKey = parseInt(key);
                if (intKey > max) max = intKey;
            }
            host.key = String(max + 1);
            this.state.task.hosts[host.key] = host;
            this.setState({ task: this.state.task, changed: true });
            this.refreshLinks();
            return;
        }
    },

    hideComponents() {
        var dialog = this.state.dialog;
        dialog.editedHost = null;
        dialog.componentType = null;
        this.setState({ dialog: dialog });
    },

    removeConnectedLinks(host) {
        for (let outputKey in host.function.outputs) {
            for (let hostKey in this.state.task.hosts) {
                let connectedHost = this.state.task.hosts[hostKey];
                for (let inputKey in connectedHost.function.inputs) {
                    let dataSource = connectedHost.function.inputs[inputKey].dataSource;
                    if (dataSource && dataSource.hostKey == host.key && dataSource.variableKey == outputKey) {
                        connectedHost.function.inputs[inputKey].dataSource = null;
                    }
                }
            }
        }
    },

    assignFunction(fce) {
        if (this.state.dialog.editedHost) {
            // remove links
            if (this.state.dialog.editedHost.function && this.state.dialog.editedHost.function.id != fce.id) {
                this.removeConnectedLinks(this.state.dialog.editedHost);
            }
            this.state.dialog.editedHost.function = fce;
            this.setState({ task: this.state.task, changed: true });
            this.refreshLinks();

            if (this.state.dialog.editedHost.device || this.state.dialog.componentType == "processors") {
                if (!this.state.dialog.editedHost.key) this.addHost(this.state.dialog.editedHost);
                this.hideComponents();
            } else {
                this.showComponents(this.state.dialog.editedHost, "devices");
            }
        } else {
            this.hideComponents();
        }
    },

    indicateTaskChange() {
        this.setState({ task: this.state.task, changed: true });
        this.refreshLinks();
    },

    populateDataSource(dataSource, hosts) {
        if (hosts[dataSource.hostKey] && hosts[dataSource.hostKey].function) {
            dataSource.variableDescription = hosts[dataSource.hostKey].function.outputs[dataSource.variableKey].description;
            dataSource.functionDescription = hosts[dataSource.hostKey].function.description;
            dataSource.deviceDescription = hosts[dataSource.hostKey].device ? hosts[dataSource.hostKey].device.description : null;
        }

        return dataSource;
    },

    selectedInputChanged: function (host, key) {
            if (this.state.selectedInput && (this.state.selectedInput.hostKey == host.key && this.state.selectedInput.variableKey == key)) {
                // clear selected input
                this.setState({ selectedInput: null });
            } else {
                if (this.state.selectedOutput && isEqualVariableTypes(this.state.selectedOutput.variable, host.function.inputs[key])) {
                    // join with output
                    host.function.inputs[key].dataSource = this.populateDataSource(this.state.selectedOutput, this.state.task.hosts);
                    this.setState({ selectedInput: null, selectedOutput: null, task: this.state.task, changed: true });
                } else {
                    // select input
                    this.setState({ selectedOutput: null, selectedInput: { hostKey: host.key, variableKey: key, variable: host.function.inputs[key] }, task: this.state.task });
                }
            }
    },

    selectedOutputChanged(host, key) {
        if (this.state.selectedOutput && (this.state.selectedOutput.hostKey == host.key && this.state.selectedOutput.variableKey == key)) {
            // clear selected output
            this.setState({ selectedOutput: null });
        } else {
            if (this.state.selectedInput && isEqualVariableTypes(this.state.selectedInput.variable, host.function.outputs[key])) {
                // join with input
                this.state.task.hosts[this.state.selectedInput.hostKey].function.inputs[this.state.selectedInput.variableKey].dataSource = this.populateDataSource({ hostKey: host.key, variableKey: key }, this.state.task.hosts);
                this.setState({ selectedInput: null, selectedOutput: null, task: this.state.task, changed: true });
            } else {
                // select output
                this.setState({ selectedInput: null, selectedOutput: { hostKey: host.key, variableKey: key, variable: host.function.outputs[key] }, task: this.state.task });
            }
        }
    },

    unlink(host, key) {
        if (host.function.inputs[key].dataSource) host.function.inputs[key].dataSource = null;
        this.setState({ task: this.state.task, changed: true });
            this.refreshLinks();
    },

    refreshLinks() {
        var ranks = {};
        for (var hostKey in this.state.task.hosts) {
            for (var variableKey in this.state.task.hosts[hostKey].function.inputs) {
                var dataSource = this.state.task.hosts[hostKey].function.inputs[variableKey].dataSource;
                if (dataSource) {
                    let key = dataSource.hostKey + "_" + dataSource.variableKey;
                    if (!ranks[key]) ranks[key] = [];
                    ranks[key].push({dataSource: dataSource, hostKey: hostKey, variableKey: variableKey });
                }
            }
        }

        var rankKeys = Object.keys(ranks);
        rankKeys.sort();
        var linkRank = -1;
        var linkCount = 0;
        var links = rankKeys.map((key) => {
            linkRank++;
            return ranks[key].map((linkInfo) => {
                return (<InputLink className={"color-" + linkRank % this.maxLinkColors} key={linkCount++} dataSource={linkInfo.dataSource} hostKey={linkInfo.hostKey} variableKey={linkInfo.variableKey} />);
            });
        });

        var merged = [].concat.apply([], links);

        this.setState({ links: merged });
    },

    getLinkRank(outputHostKey, outputVariableKey) {
        var ranks = {};
        for (var hostKey in this.state.task.hosts) {
            for (var variableKey in this.state.task.hosts[hostKey].function.inputs) {
                var dataSource = this.state.task.hosts[hostKey].function.inputs[variableKey].dataSource;
                if (dataSource) {
                    ranks[dataSource.hostKey + "_" + dataSource.variableKey] = 1;
                }
            }
        }
        var key = outputHostKey + "_" + outputVariableKey;
        if (!ranks[key]) return null;

        var rankKeys = Object.keys(ranks);
        rankKeys.sort();
        return rankKeys.indexOf(key) % this.maxLinkColors;
    },

    toggleParameters: function (host) {
        host.showAllParameters = !host.showAllParameters;
        this.setState({ task: this.state.task });
        this.refreshLinks();
    },

    createHostsTable(hosts) {

        //all table rows
        var hostsRows = [];

        var totalRows = 0;
        if (hosts) {

            var producerKeys = Object.keys(hosts).filter((key) => {
                var host = hosts[key];
                if (host.function) {
                    return !this.hasProperties(host.function.inputs) && this.hasProperties(host.function.outputs);
                }
                return false;
            });

            // get producers
            var producerKeys = Object.keys(hosts).filter((key) => {
                var host = hosts[key];
                if (host.function) {
                    return !this.hasProperties(host.function.inputs) && this.hasProperties(host.function.outputs);
                }
                return false;
            });

            // get processors
            var processorKeys = Object.keys(hosts).filter((key) => {
                var host = hosts[key];
                if (host.function) {
                    return this.hasProperties(host.function.inputs) && this.hasProperties(host.function.outputs);
                }
                return false;
            });

            // get consumers
            var consumerKeys = Object.keys(hosts).filter((key) => {
                var host = hosts[key];
                if (host.function) {
                    return this.hasProperties(host.function.inputs) && !this.hasProperties(host.function.outputs);
                }
                return false;
            });

            // get maximal number of hosts in column (producers, processors or consumers)
            var maxHosts = Math.max.apply(null, [producerKeys.length, processorKeys.length, consumerKeys.length]);

            // fiter invisible parameters with default value (boolean has always default value)
            var createVisibleParameterKeysFilter = function (host) {
                return function (key) {
                    return host.showAllParameters || (host.function.parameters[key].type != 0 && host.function.parameters[key].defaultValue === null);
                }
            };

            for (let i = 0; i < maxHosts; i++) {
                // compose host row
                let hostsKeysRow = [];
                hostsKeysRow.push((i < producerKeys.length) ? producerKeys[i] : null);
                hostsKeysRow.push((i < processorKeys.length) ? processorKeys[i] : null);
                hostsKeysRow.push((i < consumerKeys.length) ? consumerKeys[i] : null);

                // get maximal number of inputs
                let maxInputs = Math.max.apply(null, hostsKeysRow.map((key) => {
                    return (key !== null && hosts[key].function && hosts[key].function.inputs) ? Object.keys(hosts[key].function.inputs).length : 0
                }));
                // get maximal number of parameters
                let maxParameters = Math.max.apply(null, hostsKeysRow.map((key) => {
                    return (key !== null && hosts[key].function && hosts[key].function.parameters) ? Object.keys(hosts[key].function.parameters).filter(createVisibleParameterKeysFilter(hosts[key])).length : 0
                }));
                // get maximal number of outputs
                let maxOutputs = Math.max.apply(null, hostsKeysRow.map((key) => {
                    return (key !== null && hosts[key].function && hosts[key].function.outputs) ? Object.keys(hosts[key].function.outputs).length : 0
                }));

                // prepare row for all hots in the same row group = table body
                let hostsItemsRow = [];

                // prepare remove row
                let removals = hostsKeysRow.map((key, index) => {
                    if (key === null) return <td className="empty-host"></td>;
                    var host = hosts[key];
                    return (
                        <td className="content-right">
                                <i onClick={this.removeHost.bind(this, host)} className="transparent-button fa fa-trash" aria-hidden="true"></i>
                        </td>
                    );
                });
                hostsItemsRow.push(<tr key={totalRows++}>{removals}</tr>);

                // prepare devices row
                let devices = hostsKeysRow.map((key, index) => {
                    if (key === null) return <td className="empty-host"></td>;
                    var host = hosts[key];
                    return (
                        <td>
                            {host.device &&
                                <Form>
                                    <Form.Input className="readonly" icon="pencil" value={host.device && host.device.description} label={tr("Device")} onClick={this.showComponents.bind(this, host, "devices")} />
                                </Form>
                            }
                        </td>
                    );
                });
                hostsItemsRow.push(<tr key={totalRows++}>{devices}</tr>);

                // prepare functions row
                let functions = hostsKeysRow.map((key, index) => {
                    if (key === null) return <td className="empty-host"></td>;
                    var host = hosts[key];
                    switch (index) {
                        case 0: var type = "producers"; break;
                        case 1: var type = "processors"; break;
                        case 2: var type = "consumers"; break;
                    }
                    return (
                        <td>
                            {host.function &&
                                <Form>
                                <Form.Input className="readonly" icon="pencil" label={tr("Function")} value={host.function && host.function.description} onClick={this.showComponents.bind(this, host, type)} />
                                </Form>
                            }
                        </td>
                    );
                });
                hostsItemsRow.push(<tr key={totalRows++}>{functions}</tr>);

                // prepare inputs header row
                let inputsHeader = hostsKeysRow.map((key) => {
                    if (key === null) return <td className="empty-host"></td>;
                    var host = hosts[key];
                    return (
                        <td>
                            {host &&
                                <Form>
                                <div className="field">
                                    <label className="green">{tr("Inputs")}</label>
                                    </div>
                                </Form>
                            }
                        </td>
                    );
                });
                hostsItemsRow.push(<tr key={totalRows++}>{inputsHeader}</tr>);

                // prepare inputs rows
                for (let j = 0; j < maxInputs; j++) {
                    let inputs = hostsKeysRow.map((hostKey, index) => {
                        if (hostKey === null) return <td className="empty-host"></td>;
                        var host = hosts[hostKey];
                        var className = "";
                        var color = null;
                        var basic = true;
                        var iconClassName = "link-endpoint";
                        if (host.function.inputs && j < Object.keys(host.function.inputs).length) {
                            var inputKeys = Object.keys(host.function.inputs);
                            inputKeys.sort();
                            var key = inputKeys[j];

                            if (this.state.selectedOutput && isEqualVariableTypes(this.state.selectedOutput.variable, host.function.inputs[key]) &&
                                hostKey != this.state.selectedOutput.hostKey && !host.function.inputs[key].dataSource) {
                                className += ' selected';
                                color = "orange";
                            }
                            if (this.state.selectedInput && this.state.selectedInput.variableKey == key && hostKey == this.state.selectedInput.hostKey) {
                                className += ' selected';
                                color = "orange";
                                basic = false;
                            }
                            var id = "input_" + hostKey + "_" + key;

                            if (host.function.inputs[key].dataSource) {
                                iconClassName += " connected";
                                iconClassName += " color-" + this.getLinkRank(host.function.inputs[key].dataSource.hostKey, host.function.inputs[key].dataSource.variableKey);
                            }

                            if (host.function.inputs[key].dataSource) {
                                let variable = this.state.task.hosts[String(host.function.inputs[key].dataSource.hostKey)].function.outputs[host.function.inputs[key].dataSource.variableKey];
                                var content = variable.description ? variable.description : host.function.inputs[key].dataSource.variableKey;
                            } else {
                                var content = host.function.inputs[key].description ? host.function.inputs[key].description : key;
                            }

                            return <td>
                                <Button fluid icon labelPosition='left' basic={basic} color={color} className={className} onClick={!host.function.inputs[key].dataSource ? this.selectedInputChanged.bind(this, host, key) : this.unlink.bind(this, host, key)}>
                                    <Icon id={id} className="plugicon">
                                        <svg className={iconClassName} x="0px" y="0px" viewBox="0 0 1000 1000">                                            <g>                                                <path d="M322.8,500c0,28.8,23.4,52.1,52.1,52.1S427,528.8,427,500s-23.4-52.1-52.1-52.1S322.8,471.2,322.8,500z M573,500c0,28.8,23.4,52.1,52.1,52.1s52.1-23.4,52.1-52.1s-23.4-52.1-52.1-52.1S573,471.2,573,500z M10,906.6c0,46.1,37.3,83.4,83.4,83.4h813.2c46.1,0,83.4-37.3,83.4-83.4l0-813.2c0-46.1-37.3-83.4-83.4-83.4L93.4,10C47.3,10,10,47.3,10,93.4V906.6L10,906.6z M218.5,500c0-155.5,125.9-281.5,281.5-281.5S781.5,344.5,781.5,500S655.5,781.5,500,781.5S218.5,655.5,218.5,500z" />                                            </g>                                        </svg>
                                    </Icon>
                                    {content}
                                </Button>
                            </td >;
                        } else {
                            return <td></td>;
                        }

                    });
                    hostsItemsRow.push(<tr key={totalRows++}>{inputs}</tr>);
                }

                // prepare parameters header row
                let parametersHeader = hostsKeysRow.map((key) => {
                    if (key === null) return <td className="empty-host"></td>;
                    var host = hosts[key];
                    return (
                        <td>
                            {host &&
                                <Form>
                                <div className="field">
                                    <label className="label-with-toggle green">{tr("Parameters")} {host && <Checkbox toggle className="green" checked={host.showAllParameters ? false : true} onChange={this.toggleParameters.bind(this, host)} label={tr("Mandatory only")} />}</label>
                                    </div>
                                </Form>
                            }
                        </td>
                    );
                });
                hostsItemsRow.push(<tr key={totalRows++}>{parametersHeader}</tr>);

                // prepare parameters rows
                for (let j = 0; j < maxParameters; j++) {
                    let parameters = hostsKeysRow.map((hostKey, index) => {
                        if (hostKey === null) return <td className="empty-host"></td>;
                        var host = hosts[hostKey];
                        var className = "";
                        var color = null;
                        var basic = true;
                        if (host.function.parameters) {
                            var filteredParameterKeys = Object.keys(host.function.parameters).filter(createVisibleParameterKeysFilter(host));
                            if (j < filteredParameterKeys.length) {
                                filteredParameterKeys.sort();
                                var key = filteredParameterKeys[j];
                                return (
                                    <td>
                                        <FunctionVariableSetter key={key} id={key} variable={host.function.parameters[key]} valueChanged={this.indicateTaskChange} />
                                    </td>
                                );
                            }
                        }
                        return <td></td>;
                    });
                    hostsItemsRow.push(<tr key={totalRows++}>{parameters}</tr>);
                }

                // prepare outputs header row
                let outputsHeader = hostsKeysRow.map((key) => {
                    if (key === null) return <td className="empty-host"></td>;
                    var host = hosts[key];
                    return (
                        <td>
                            {host &&
                                <Form>
                                    <div className="field">
                                    <label className="green">{tr("Outputs")}</label>
                                    </div>
                                </Form>
                            }
                        </td>
                    );
                });
                hostsItemsRow.push(<tr key={totalRows++}>{outputsHeader}</tr>);

                // prepare outputs rows
                for (let j = 0; j < maxOutputs; j++) {
                    let outputs = hostsKeysRow.map((hostKey, index) => {
                        if (hostKey === null) return <td className="empty-host"></td>;
                        var host = hosts[hostKey];
                        var className = "";
                        var iconClassName = "link-endpoint";
                        var color = null;
                        var basic = true;
                        if (host.function.outputs && j < Object.keys(host.function.outputs).length) {
                            var outputKeys = Object.keys(host.function.outputs);
                            outputKeys.sort();
                            var key = outputKeys[j];

                            if (this.state.selectedInput && isEqualVariableTypes(this.state.selectedInput.variable, host.function.outputs[key]) && hostKey != this.state.selectedInput.hostKey) {
                                className += ' selected';
                                color = "orange";
                            }
                            if (this.state.selectedOutput && this.state.selectedOutput.variableKey == key && hostKey == this.state.selectedOutput.hostKey) {
                                className += ' selected';
                                color = "orange";
                                basic = false;
                                iconClassName += " connected";
                            }

                            var id = "output_" + hostKey + "_" + key;

                            var linkRank = this.getLinkRank(hostKey, key);

                            if (linkRank >= 0) {
                                iconClassName += " connected";
                                iconClassName += " color-" + linkRank;
                            }

                            var content = host.function.outputs[key].description ? host.function.outputs[key].description : key;

                            return (
                                <td>
                                    <Button fluid icon labelPosition='right' basic={basic} color={color} onClick={this.selectedOutputChanged.bind(this, host, key)} className={className}>
                                        {content}
                                        <Icon id={id} className="plugicon">
                                            <svg className={iconClassName} x="0px" y="0px" viewBox="0 0 1000 1000">                                                <g transform="rotate(90 500,500) translate(0.000000,512.000000) scale(0.100000,-0.100000)">                                                    <path d="M3043.8,3551.9l-5.7-1470l-983.8-5.7l-981.9-3.8l9.6-685.2c9.6-723.5,24.9-876.6,118.7-1249.9c239.3-939.8,815.4-1755.2,1627-2296.9c245-164.6,656.5-363.7,947.5-457.5l248.8-82.3v-1039.3V-4780H5000h976.2v1041.3v1039.3l245,80.4c581.9,189.5,1108.3,511,1544.7,949.4c604.8,602.9,974.3,1332.2,1112.1,2191.6c23,149.3,34.5,375.2,40.2,874.7l9.6,675.7l-981.9,3.8l-983.8,5.7l-5.7,1470l-3.8,1468.1h-488.1h-488.1V3546.2V2072.3H5000h-976.2v1473.8V5020h-488.1h-488.1L3043.8,3551.9z" />                                                </g>                                            </svg>
                                        </Icon>
                                    </Button>
                                </td>
                            )
                        } else {
                            return <td></td>;
                        }

                    });
                    hostsItemsRow.push(<tr key={totalRows++}>{outputs}</tr>);
                }

                hostsRows.push(<tbody>{hostsItemsRow}</tbody>);
            }
        }

        return (
            <table className="full-width tasks">
                <thead>
                    <tr><td></td><th>{tr("Task schema")}</th><td></td></tr>
                    <tr>
                        <th>{tr("Producers")}</th>
                        <th>{tr("Processors")}</th>
                        <th>{tr("Consumers")}</th>
                    </tr>
                </thead>
                {hostsRows}
                <tfoot>
                    <tr>
                        <td><Button fluid onClick={this.showComponents.bind(this, {}, "producers")}>{tr("Add producer")}</Button></td>
                        <td><Button fluid onClick={this.showComponents.bind(this, {}, "processors")}>{tr("Add processor")}</Button></td>
                        <td><Button fluid onClick={this.showComponents.bind(this, {}, "consumers")}>{tr("Add consumer")}</Button></td>
                    </tr>
                </tfoot>
            </table>
        );
    },

    hostSorter(a, b) {
        if (!this.state.task.hosts[a].function.inputs && this.state.task.hosts[b].function.inputs) return -1;
        if (this.state.task.hosts[a].function.inputs && !this.state.task.hosts[b].function.inputs) return 1;

        if (!this.state.task.hosts[a].function.outputs && this.state.task.hosts[b].function.outputs) return 1;
        if (this.state.task.hosts[a].function.outputs && !this.state.task.hosts[b].function.outputs) return -1;

        return 0;
    },

    render: function () {
        var saveButton = (this.state.changed) ? <Button color="green" onClick={this.saveTask}>{tr("Save")}</Button> : <Button onClick={this.saveTask}>{tr("Save")}</Button>;

        var openModal = this.state.dialog.componentType != null;

        var returnLink = this.props.match.url.replace(/(.*\/task)\/\w+$/i, '$1');
        if (!returnLink) returnLink = '/';

        var parametersKey = 0;
        var parameters = [];
        if (this.state.task && this.state.task.hosts) {
            var parametersKeys = Object.keys(this.state.task.hosts);
            parametersKeys.sort(this.hostSorter);
            parameters = parametersKeys.map((hostKey) => {
                var host = this.state.task.hosts[hostKey];
                if (!(host.function && host.function.parameters)) return [];
                var functionParametersKey = 0;
                var functionParameters = Object.keys(host.function.parameters).filter((parameterKey) => {
                    return host.function.parameters[parameterKey].type != 0 && host.function.parameters[parameterKey].defaultValue === null;
                }).map((parameterKey) => {
                    return (<FunctionVariableSetter key={functionParametersKey++} id={parameterKey} variable={host.function.parameters[parameterKey]} valueChanged={this.indicateTaskChange} />);
                });

                if (functionParameters.length == 0) return functionParameters;

                return (
                    <div key={parametersKey++}>
                        {(host.device || host.function) &&
                            <div className="host-header">{host.device && <span>{host.device.description} <i className="fa fa-long-arrow-right" aria-hidden="true"></i></span>} {host.function && host.function.description}</div>
                        }
                        {functionParameters}
                    </div>
                );

            });
        }

        var flattenedParameters = [].concat.apply([], parameters);

        return (
            <div>
                <header className="main-header">
                    <div className="main-column">
                        <h2 className="backlink"><NavLink to={{ pathname: returnLink }}><i className="fa fa-long-arrow-left" aria-hidden="true"></i> {tr("Tasks list")}</NavLink></h2>
                        <div className="left-header">
                            <h1>{tr("Task settings")}</h1>
                            <div className="menu-buttons green">
                                <Checkbox toggle className="green" checked={this.state.task.active} onChange={this.changeTaskActive} label={tr("Active")} />
                                {saveButton}
                                {this.state.task.stored &&
                                    <Button onClick={this.copyTask} secondary >{tr("Copy task")}</Button>
                                }
                            </div>
                        </div>
                        <div className="list-item-detail">
                            <div className="id"><span>{this.state.task.name}</span></div>
                        </div>
                    </div>
                </header>
                <div className="container body-content main-column">
                    {this.state.error &&
                        <Message negative>
                            <Message.Header>Error</Message.Header>
                            {this.state.error}
                        </Message>
                    }

                    <div className="box horizontal">
                        <Form className="fill column">
                            <Form.Input value={this.state.task.name ? this.state.task.name : ''} label={tr("Name")} onChange={this.changeTaskName} />
                            <Form.TextArea value={this.state.task.description ? this.state.task.description : ''} label={tr("Description")} onChange={this.changeTaskDescription} />
                        </Form>
                        <Form className="fill column">
                            {flattenedParameters && flattenedParameters.length > 0 &&
                                <div>
                                    <h3>{tr("Mandatory parameters")}</h3>
                                    {flattenedParameters}
                                </div>
                            }
                        </Form>
                    </div>

                    {this.createHostsTable(this.state.task.hosts)}

                    {this.state.links}

                    <Modal open={openModal}>
                        <Modal.Header>{this.state.dialog.header}</Modal.Header>
                        <Modal.Content>
                            {this.state.dialog.components}
                        </Modal.Content>
                        <Modal.Actions>
                            <Button onClick={this.hideComponents}>{tr("Close")}</Button>
                        </Modal.Actions>
                    </Modal>
                </div>
            </div>
        );
    }
});

module.exports = TaskDetail;
