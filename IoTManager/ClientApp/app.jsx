﻿import 'core-js/es6/map';
import 'core-js/es6/set';
//import "babel-polyfill";
var React = require('react');
var createReactClass = require('create-react-class');
import * as ReactDOM from 'react-dom';
var DevicesList = require('./devices-list')
var DeviceDetail = require('./device-detail')
var TaskDetail = require('./task-detail')
var FunctionsList = require('./functions-list')
var FunctionDetail = require('./function-detail')
var TasksList = require('./tasks-list')
import { HashRouter, Route, Switch, NavLink } from 'react-router-dom'
import { setLanguage, tr } from './components/translation';
import { Dropdown } from 'semantic-ui-react'

setLanguage(globalLanguage);

var LanguagesMenu = createReactClass({

    render: function () {

        return (
            <Dropdown text={globalLanguage}>
                <Dropdown.Menu>
                    <Dropdown.Item ><a href="home.index?culture=en-GB&ui-culture=en-GB">en</a></Dropdown.Item>
                    <Dropdown.Item ><a href="home.index?culture=cs-CZ&ui-culture=cs-CZ">cs</a></Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        );
    }
});

var App = createReactClass({

    render: function () {

        return (
            <HashRouter>
                <div>
                    <nav className="main-menu">
                        <ul className="left-menu">
                            <li><NavLink to="/device" activeClassName="active">{tr("Devices")}</NavLink></li>
                            <li><NavLink to="/function" activeClassName="active">{tr("Functions")}</NavLink></li>
                            <li><NavLink to="/task" activeClassName="active">{tr("Tasks")}</NavLink></li>
                        </ul>
                        <ul className="right-menu">
                            <li>{userData.Name}</li>
                            <li><a href="Home.Logout">{tr("Logout")}</a></li>
                            <li id="languages-menu">
                                <Dropdown text={globalLanguage}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item ><a href="home.index?culture=en-GB&ui-culture=en-GB">en</a></Dropdown.Item>
                                        <Dropdown.Item ><a href="home.index?culture=cs-CZ&ui-culture=cs-CZ">cs</a></Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route path='/' exact component={DevicesList} />
                        <Route path='/device' exact component={DevicesList} />
                        <Route path='/device/:id' exact component={DeviceDetail} />
                        <Route path='/device/:id/task' exact component={DeviceDetail} />
                        <Route path='/device/:deviceId/task/:id' component={TaskDetail} />
                        <Route path='/function' exact component={FunctionsList} />
                        <Route path='/function/:id' component={FunctionDetail} />
                        <Route path='/task' exact component={TasksList} />
                        <Route path='/task/:id' component={TaskDetail} />
                    </Switch>
                </div>
            </HashRouter>
        );
    }
});

if (document.getElementById('languages-menu')) {
    ReactDOM.render(<LanguagesMenu />, document.getElementById('languages-menu'));
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
