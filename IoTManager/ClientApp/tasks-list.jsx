﻿var React = require('react');
var createReactClass = require('create-react-class');
var AggregatedCounts = require('./components/aggregatedCounts');
var TaskRow = require('./components/taskRow');
import { Button, Table, Input, Dropdown, Label } from 'semantic-ui-react'
import { Map } from 'immutable'
import { NavLink } from 'react-router-dom'
import { tr } from './components/translation';

var TasksList = createReactClass({

    getInitialState: function () {
        return { tasks: [], checked: Map({}), counts: { active: 0, inactive: 0 }, filter: Map({ active: true, inactive: true, text: '', lines: 5 }), timeoutId: null };
    },

    getPollInterval() {
        return this.props.pollInterval ? this.props.pollInterval : 2000;
    },

    componentDidMount: function () {
        this.loadTasks();
    },

    componentWillUnmount: function () {
        if (this.state.timeoutId !== null) clearTimeout(this.state.timeoutId);
    },

    checkedChangedHandler: function (id) {
        let value = this.state.checked.get(id);
        this.setState({ checked: this.state.checked.set(id, !value) });
    },

    activeChangedHandler: function (task) {
        this.setState({ task: this.state.task });

        $.ajax({
            url: "task.active",
            cache: false,
            context: this,
            data: { id: task.id, active: task.active }
        })
            .done(function (tasks) {
                this.updateTasks(tasks);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    updateTasks(tasks) {
        if (tasks && Array.isArray(tasks)) {
            this.setState({ tasks: tasks });
            var counts = { active: 0, inactive: 0 };
            tasks.forEach((task) => {
                if (task.active) counts.active++;
                else counts.inactive++;
            });
            this.setState({ counts: counts });
        }
    },

    loadTasks: function () {
        $.ajax({
            url: "task.list",
            cache: false,
            context: this,
            data: {}
        })
            .done(function (tasks) {
                this.updateTasks(tasks);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
                this.setState({ timeoutId: setTimeout(this.loadTasks, this.getPollInterval()) });
            });
    },

    deleteSelected: function () {
        this.state.checked.map((value, index) => {
            if (value) this.deleteTask(index);
            this.setState({ state: this.state.checked.delete(index) });
        })
    },

    deleteTask: function (id) {
        $.ajax({
            url: "task.delete",
            cache: false,
            context: this,
            data: {id: id}
        })
            .done(function (tasks) {
                this.updateTasks(tasks);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    toggleShowAll() {
        let filter = this.state.filter;
        filter = filter.set('lines', (filter.get('lines') < 0) ? 5 : -1);
        this.setState({ filter: filter });
    },

    filterActive() {
        var filter = this.state.filter;
        filter = filter.set('active', true);
        filter = filter.set('inactive', false);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterInactive() {
        var filter = this.state.filter;
        filter = filter.set('active', false);
        filter = filter.set('inactive', true);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterReset() {
        var filter = this.state.filter;
        filter = filter.set('active', true);
        filter = filter.set('inactive', true);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterByText(event, { value }) {
        var filter = this.state.filter;
        filter = filter.set('active', true);
        filter = filter.set('inactive', true);
        filter = filter.set('text', value);
        this.setState({ filter: filter });
    },

    render: function () {

        var lines = 0;
        var rows = this.state.tasks.filter(function (task) {
            if (!((task.active && this.state.filter.get('active')) ||
                (!task.active && this.state.filter.get('inactive')))) {
                return false;
            }

            if (this.state.filter.get('text') && (task.description || task.name)) {
                if (!(task.description && task.description.toLowerCase().search(this.state.filter.get('text')) != -1) &&
                    !(task.name && task.name.toLowerCase().search(this.state.filter.get('text')) != -1)) {
                    return false;
                }
            }

            lines++;
            return (this.state.lines >= 0 && lines > this.state.lines) ? false : true;

        }, this)
        .map((task, index) =>
            <TaskRow key={task.id} task={task} checked={this.state.checked.get(task.id) ? true : false} checkedChanged={this.checkedChangedHandler} activeChanged={this.activeChangedHandler} />
        );

        return (
            <div>
                <header className="main-header">
                    <div className="main-column">
                        <div className="left-header">
                            <h1>{tr("Tasks list")}</h1>
                            <div className="menu-buttons green">
                                <NavLink to={{ pathname: '/task/new' }}><Button secondary >{tr("New task")}</Button></NavLink>
                                <Dropdown icon={null} trigger={<span><Input icon='search' placeholder={tr("Filter")} onChange={this.filterByText} /></span>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.filterActive}><Label circular color="green" empty /> {tr("Active")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterInactive}><Label circular color="red" empty /> {tr("Inactive")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterReset}>{tr("Reset filter")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.deleteSelected}>{tr("Delete selected tasks")}</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                        
                        <div className="header-aggregated-values">
                            <AggregatedCounts total={this.state.counts.active + this.state.counts.inactive} count1={this.state.counts.active} count2={this.state.counts.inactive} />
                        </div>
                    </div>
                </header>
                <header className="sub-header main-column">
                    <div className="header-aggregated-values">
                        <span className="box">{tr("Tasks")}</span>
                        <div className="detail-values">
                            <span className="box">{tr("Active")}</span>
                            <span className="box">{tr("Inactive")}</span>
                        </div>
                    </div>
                </header>
                <div className="container body-content main-column">
                    <Table striped>
                        {rows}
                    </Table>
                    <div className="footer">
                        <Button onClick={this.toggleShowAll}>
                            {(this.state.filter.get('lines') < 0) ? tr("Show") + " 5" : tr("Show all")}
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = TasksList;
