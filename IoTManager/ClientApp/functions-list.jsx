﻿var React = require('react');
var createReactClass = require('create-react-class');
var AggregatedCounts = require('./components/aggregatedCounts');
var FunctionRow = require('./components/functionRow');
import { hasRight } from './components/userRights'
import { Button, Table, Input, Dropdown, Label } from 'semantic-ui-react'
import { Map } from 'immutable'
import { NavLink } from 'react-router-dom'
import { tr } from './components/translation';

var FunctionsList = createReactClass({

    getInitialState: function () {
        return { functions: [], checked: Map({}), counts: { producers: 0, consumers: 0, processors: 0 }, filter: Map({ producers: true, consumers: true, processors: true, text: '', lines: 5 }), timeoutId: null };
    },

    getPollInterval() {
        return this.props.pollInterval ? this.props.pollInterval : 2000;
    },

    componentDidMount: function () {
        this.loadFunctions();
    },

    componentWillUnmount: function () {
        if (this.state.timeoutId !== null) clearTimeout(this.state.timeoutId);
    },

    checkedChangedHandler: function (id) {
        let value = this.state.checked.get(id);
        this.setState({ checked: this.state.checked.set(id, !value) });
    },

    hasProperties(object) {
        if (!object) return false;
        return Object.keys(object).length > 0;
    },

    updateFunctions(functions) {
        if (functions && Array.isArray(functions)) {
            this.setState({ functions: functions });
            var counts = { producers: 0, consumers: 0, processors: 0 };
            functions.forEach((fce) => {
                if (this.hasProperties(fce.inputs) && this.hasProperties(fce.outputs)) counts.processors++;
                else {
                    if (this.hasProperties(fce.inputs)) counts.consumers++;
                    if (this.hasProperties(fce.outputs)) counts.producers++;
                }
            });
            this.setState({ counts: counts });
        }
    },

    loadFunctions: function () {
        $.ajax({
            url: "function.list",
            cache: false,
            context: this,
            data: {}
        })
            .done(function (functions) {
                this.updateFunctions(functions);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
                this.setState({ timeoutId: setTimeout(this.loadFunctions, this.getPollInterval()) });
            });
    },

    deleteSelected: function () {
        this.state.checked.map((value, index) => {
            if (value) this.deleteFunction(index);
            this.setState({ state: this.state.checked.delete(index) });
        })
    },
    
    deleteFunction: function (id) {
        $.ajax({
            url: "function.delete",
            cache: false,
            context: this,
            data: {id: id}
        })
            .done(function (functions) {
                this.updateFunctions(functions);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    toggleShowAll() {
        let filter = this.state.filter;
        filter = filter.set('lines', (filter.get('lines') < 0) ? 5 : -1);
        this.setState({ filter: filter });
    },

    filterProducers() {
        var filter = this.state.filter;
        filter = filter.set('producers',  true);
        filter = filter.set('consumers',  false);
        filter = filter.set('processors', false);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterConsumers() {
        var filter = this.state.filter;
        filter = filter.set('producers',  false);
        filter = filter.set('consumers',  true);
        filter = filter.set('processors',  false);
        filter = filter.set('text',  '');
        this.setState({ filter: filter });
    },

    filterProcessors() {
        var filter = this.state.filter;
        filter = filter.set('producers',  false);
        filter = filter.set('consumers',  false);
        filter = filter.set('processors',  true);
        filter = filter.set('text',  '');
        this.setState({ filter: filter });
    },

    filterReset() {
        var filter = this.state.filter;
        filter = filter.set('producers',  true);
        filter = filter.set('consumers',  true);
        filter = filter.set('processors',  true);
        filter = filter.set('text',  '');
        this.setState({ filter: filter });
    },

    filterByText(event, { value }) {
        var filter = this.state.filter;
        filter = filter.set('producers',  true);
        filter = filter.set('consumers',  true);
        filter = filter.set('processors',  true);
        filter = filter.set('text', value);
        this.setState({ filter: filter });
    },

    render: function () {

        var lines = 0;
        var rows = this.state.functions.filter(function (fce) {
            if (!((!this.hasProperties(fce.inputs) && this.hasProperties(fce.outputs) && this.state.filter.get('producers')) ||
                (this.hasProperties(fce.inputs) && !this.hasProperties(fce.outputs) && this.state.filter.get('consumers')) ||
                (this.hasProperties(fce.inputs) && this.hasProperties(fce.outputs) && this.state.filter.get('processors')) ||
                (this.state.filter.get('producers') && this.state.filter.get('consumers') && this.state.filter.get('processors'))
            )) {
                return false;
            }

            if (this.state.filter.get('text')) {
                if (!((fce.description && fce.description.toLowerCase().search(this.state.filter.get('text')) != -1) ||
                    (fce.description && (fce.description.search(this.state.filter.get('text')) != -1)))) return false;
            }

            lines++;
            return (this.state.filter.get('lines') >= 0 && lines > this.state.filter.get('lines')) ? false : true;

        }, this)
            .map((fce, index) =>
                <FunctionRow key={fce.id} function={fce} checked={this.state.checked.get(fce.id) ? true : false} checkedChanged={this.checkedChangedHandler} />
            );

        return (
            <div>
                <header className="main-header">
                    <div className="main-column">
                        <div className="left-header">
                            <h1>{tr("Functions list")}</h1>
                            <div className="menu-buttons green">
                                {hasRight('developer') &&
                                    <NavLink to={{ pathname: '/function/new' }}><Button secondary >{tr("New function")}</Button></NavLink>
                                }
                                <Dropdown icon={null} trigger={<span><Input icon='search' placeholder={tr("Filter")} onChange={this.filterByText} /></span>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.filterProducers}><Label circular color="green" empty /> {tr("Producers")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterConsumers}><Label circular color="red" empty /> {tr("Consumers")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterProcessors}><Label circular color="yellow" empty /> {tr("Processors")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterReset}>{tr("Reset filter")}</Dropdown.Item>
                                        {(hasRight('administrator') || hasRight('developer')) && <Dropdown.Item onClick={this.deleteSelected}>{tr("Delete selected functions")}</Dropdown.Item>}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                        <div className="header-aggregated-values">
                            <AggregatedCounts total={this.state.counts.producers + this.state.counts.consumers + this.state.counts.processors} count1={this.state.counts.producers} count2={this.state.counts.consumers} count3={this.state.counts.processors} />
                        </div>

                    </div>
                </header>
                <header className="sub-header main-column">
                    <div className="header-aggregated-values">
                        <span className="box">{tr("Functions")}</span>
                        <div className="detail-values">
                            <span className="box">{tr("Producers")}</span>
                            <span className="box">{tr("Consumers")}</span>
                            <span className="box">{tr("Processors")}</span>
                        </div>
                    </div>
                </header>
                <div className="container body-content main-column">
                    <Table striped>
                        {rows}
                    </Table>
                    <div className="footer">
                        <Button onClick={this.toggleShowAll}>
                            {(this.state.filter.get('lines') < 0) ? tr("Show") + " 5" : tr("Show all")}
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = FunctionsList;
