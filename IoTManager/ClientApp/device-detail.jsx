﻿var React = require('react');
var createReactClass = require('create-react-class');
var moment = require('moment');
var AggregatedCounts = require('./components/aggregatedCounts');
var TaskRow = require('./components/taskRow');
import { Menu, Form, Button, Table, Dropdown, Label, Input } from 'semantic-ui-react'
import { Map } from 'immutable'
import { NavLink } from 'react-router-dom'
import { getName } from './components/utils'
import { hasRight } from './components/userRights'
import { tr } from './components/translation';

var DeviceDetail = createReactClass({

    getRetryInterval() {
        return this.props.retryInterval ? this.props.retryInterval : 500;
    },

    getInitialState: function () {
        return { device: {}, checked: Map({}), counts: { active: 0, inactive: 0 }, filter: Map({ active: true, inactive: true, text: '', lines: 5 }), timeoutId: null, changed: false };
    },

    componentDidMount: function () {
        this.loadDevice();
    },

    componentWillUnmount: function () {
        if (this.state.timeoutId !== null) clearTimeout(this.state.timeoutId);
    },

    checkedChangedHandler: function (id) {
        let value = this.state.checked.get(id);
        this.setState({ checked: this.state.checked.set(id, !value) });
    },

    activeChangedHandler: function (task) {
        this.setState({ device: this.state.device, changed: true });

        $.ajax({
            url: "task.active",
            cache: false,
            context: this,
            data: { id: task.id, active: task.active }
        })
            .done(function (task) {
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    updateDevice(device) {
        if (device) {
            this.setState({ device: device, changed: false });
            if (device.tasks && Array.isArray(device.tasks)) {
                var counts = { active: 0, inactive: 0 };
                device.tasks.forEach((task) => {
                    if (task.active) counts.active++;
                    else counts.inactive++;
                });
                this.setState({ counts: counts });
            }
        }
    },

    loadDevice: function () {
        $.ajax({
            url: "device.detail",
            cache: false,
            context: this,
            data: { id: this.props.match.params.id }
        })
            .done(function (device) {
                this.updateDevice(device);
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    deleteSelected: function () {
        this.state.checked.map((value, index) => {
            if (value) this.deleteTask(index);
            this.setState({ state: this.state.checked.delete(index) });
        })
    },

    deleteTask: function (id) {
        if (!this.state.device.tasks) return;

        var taskPosition = -1;
        for (var i = 0; i < this.state.device.tasks.length; i++){
            if (this.state.device.tasks[i].id == id) {
                taskPosition == i;
                break;
            }
        }

        if (taskPosition != -1) {
            this.state.device.tasks.splice(taskPosition, 1);
            this.setState({ device: this.state.device });

            $.ajax({
                url: "task.delete",
                cache: false,
                context: this,
                data: { id: id }
            })
                .done(function (task) {
                })
                .fail(function () {
                    if (jqXHR.status == 401) window.location = "home.login";
                })
                .always(function () {
                });
        }
    },

    saveDevice() {
        $.ajax({
            method: 'POST',
            url: "device.edit",
            cache: false,
            context: this,
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(this.state.device)
        })
            .done(function (device) {
                this.updateDevice(device);
                var link = document.body.querySelector("h2.backlink a");
                if (link) {
                    link.click();
                }
            })
            .fail(function () {
                if (jqXHR.status == 401) window.location = "home.login";
            })
            .always(function () {
            });
    },

    changeDeviceGroup(event, { value }) {
        this.state.device.groupId = value;
        this.setState({ device: this.state.device, changed: true });
    },

    changeDeviceDescription(event, { value }) {
        if (!hasRight('administrator')) return;
        this.state.device.description = value;
        this.setState({ device: this.state.device, changed: true });
    },

    toggleShowAll() {
        let filter = this.state.filter;
        filter = filter.set('lines', (filter.get('lines') < 0) ? 5 : -1);
        this.setState({ filter: filter });
    },

    filterActive() {
        var filter = this.state.filter;
        filter = filter.set('active', true);
        filter = filter.set('inactive', false);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterInactive() {
        var filter = this.state.filter;
        filter = filter.set('active', false);
        filter = filter.set('inactive', true);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterReset() {
        var filter = this.state.filter;
        filter = filter.set('active', true);
        filter = filter.set('inactive', true);
        filter = filter.set('text', '');
        this.setState({ filter: filter });
    },

    filterByText(event, { value }) {
        var filter = this.state.filter;
        filter = filter.set('active', true);
        filter = filter.set('inactive', true);
        filter = filter.set('text', value);
        this.setState({ filter: filter });
    },

    render: function () {

        var lines = 0;
        var rows = [];
        if (this.state.device && this.state.device.tasks) {
            rows = this.state.device.tasks.filter(function (task) {
                if (!((task.active && this.state.filter.get('active')) ||
                    (!task.active && this.state.filter.get('inactive')))) {
                    return false;
                }

                if (this.state.filter.get('text')) {
                    if (!task.description && !task.name) return false;
                    if (!(task.description && task.description.toLowerCase().search(this.state.filter.get('text')) != -1) &&
                        !(task.name && task.name.toLowerCase().search(this.state.filter.get('text')) != -1)) {
                        return false;
                    }
                }
                
                lines++;
                return (this.state.filter.get('lines') >= 0 && lines > this.state.filter.get('lines')) ? false : true;

            }, this)
                .map((task, index) =>
                    <TaskRow key={task.id} task={task} checked={this.state.checked.get(task.id) ? true : false} checkedChanged={this.checkedChangedHandler} activeChanged={this.activeChangedHandler} match={this.props.match} />
                );
        }

        var lastOnline = (this.state.device.lastOnline) ? moment(this.state.device.lastOnline).format('lll') : null;

        var saveButton = (this.state.changed) ? <Button color="green" onClick={this.saveDevice}>{tr("Save")}</Button> : <Button onClick={this.saveDevice}>{tr("Save")}</Button>;

        var newTaskLink = ((this.props.match) ? this.props.match.url : '') + '/task/new';

        var allgroups = Object.keys(userData.AllGroups).map((key) => { return { key: key, value: parseInt(key), text: userData.AllGroups[key] }; });
        allgroups.push({ key: "null", value: null, text: "" });

        return (
            <div>
                <header className="main-header">
                    <div className="main-column">
                        <h2 className="backlink"><NavLink to={{ pathname: '/device' }}><i className="fa fa-long-arrow-left" aria-hidden="true"></i> {tr("Devices list")}</NavLink></h2>
                        <div className="left-header">
                            <h1>{tr("Device settings")}</h1>
                            <div className="menu-buttons green">
                                {hasRight('administrator') && saveButton}
                                <NavLink to={{ pathname: newTaskLink }}><Button secondary >{tr("New task")}</Button></NavLink>
                                <Dropdown icon={null} trigger={<span><Input icon='search' placeholder={tr("Filter")} onChange={this.filterByText} /></span>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.filterActive}><Label circular color="green" empty /> {tr("Active")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterInactive}><Label circular color="red" empty /> {tr("Inactive")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.filterReset}>{tr("Reset filter")}</Dropdown.Item>
                                        <Dropdown.Item onClick={this.deleteSelected}>{tr("Delete selected task")}</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>

                        <div className="list-item-detail">
                            <div className="id"><span>{getName(this.state.device.description)}</span></div>
                            <div>{tr("Group")}: {(this.state.device.groupId) ? userData.AllGroups[this.state.device.groupId] : ""}</div>
                            <div>{tr("Version")}: {this.state.device.version}</div>
                            <div>{tr("Last online")}: {lastOnline}</div>
                            <div>IP: {this.state.device.ip}</div>
                            <div>Firmware: {this.state.device.firmwareVersion}</div>
                            <div>UUID: {this.state.device.uuid}</div>
                            <div>{this.state.device.description}</div>
                        </div>

                        <div className="header-aggregated-values">
                            <AggregatedCounts total={this.state.counts.active + this.state.counts.inactive} count1={this.state.counts.active} count2={this.state.counts.inactive} />
                        </div>
                    </div>
                </header>
                <header className="sub-header main-column">
                    <div className="header-aggregated-values">
                        <span className="box">{tr("Tasks")}</span>
                        <div className="detail-values">
                            <span className="box">{tr("Active")}</span>
                            <span className="box">{tr("Inactive")}</span>
                        </div>
                    </div>
                </header>
                <div className="container body-content main-column">

                    <Form>
                        {hasRight('administrator') &&
                            <Form.Select options={allgroups} value={this.state.device.groupId} label={tr("Group")} onChange={this.changeDeviceGroup} />
                        }
                        <Form.TextArea value={this.state.device.description ? this.state.device.description : ''} onChange={this.changeDeviceDescription} label={tr("Description")} />
                    </Form>

                    <Table singleLine striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan="5">{tr("Tasks containing selected device")}</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        {rows}
                    </Table>
                    <div className="footer">
                        <Button onClick={this.toggleShowAll}>
                            {(this.state.filter.get('lines') < 0) ? tr("Show") + " 5" : tr("Show all")}
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = DeviceDetail;
