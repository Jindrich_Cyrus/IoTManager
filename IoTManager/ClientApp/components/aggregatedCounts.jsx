﻿var React = require('react');
var createReactClass = require('create-react-class');

var AggregatedCounts = createReactClass({
    render: function () {
        return (
            <div className="aggregated-counts">
                <span className="value aggregated-counts-total"><span className="box">{this.props.total}</span></span>
                <div className="detail-values">
                    {this.props.count1 !== undefined && this.props.count1 !== null &&
                        <span className="value type1"><span className="box">{this.props.count1}</span></span>
                    }
                    {this.props.count2 !== undefined && this.props.count2 !== null &&
                        <span className="value type2"><span className="box">{this.props.count2}</span></span>
                    }
                    {this.props.count3 !== undefined && this.props.count3 !== null &&
                        <span className="value type3"><span className="box">{this.props.count3}</span></span>
                    }
                </div>
            </div>
        );
    }
});

module.exports = AggregatedCounts;