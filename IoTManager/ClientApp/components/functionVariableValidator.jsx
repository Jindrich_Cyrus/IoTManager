﻿var validator = require('validator');

class FunctionVariableValidator {
  constructor() {
    
  }

  isValid(type, stringValue, enumValues) {
      switch (type) {
          case 0: return validator.isBoolean(String(stringValue));
          case 1: return validator.isInt(String(stringValue));
          case 2: return validator.isFloat(String(stringValue));
          case 4: return enumValues && enumValues.indexOf(String(stringValue)) != -1;
          default: return true;
      }
  }
}

// expose the class
module.exports = FunctionVariableValidator;
