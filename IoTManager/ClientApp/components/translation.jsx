﻿var translations = {
    'en': {
    },
    'cs': {
        "Devices": "Zařízení",
        "Device": "Zařízení",
        "Functions": "Funkce",
        "Tasks": "Úlohy",
        "Devices list": "Seznam zařízení",
        "Filter": "Filtrovat",
        "Updates": "Aktualizace",
        "Online": "Online",
        "Offline": "Offline",
        "Reset filter": "Zruš filtr",
        "Delete selected device": "Smaž vybrané zařízení",
        "Add device": "Přidej zařízení",
        "Description": "Popis",
        "Group": "Skupina",
        "Status": "Stav",
        "Show": "Ukaž",
        "Show all": "Ukaž vše",
        "Detail": "Detail",
        "Save": "Uložit ",
        "Device settings": "Nastavení a správa zařízení",
        "Version": "Verze",
        "Last online": "Naposledy online",
        "Active": "Aktivní",
        "Inactive": "Neaktivní",
        "Delete selected task": "Smazat vybranou úlohu",
        "New task": "Nová úloha",
        "Tasks containing selected device": "Úlohy jejichž součástí je zvolené zařízení",
        "Functions list": "Seznam funkcí",
        "Producers": "Producenti",
        "Consumers": "Konzumenti",
        "Processors": "Zpracovatelé",
        "Delete selected functions": "Smazat vybrané funkce",
        "New function": "Nová funkce",
        "Function settings": "Nastaveni a sprava funkce",
        "Local installation possible": "Možná instalace na zařízení",
        "Versions": "Verze",
        "Name": "Jméno",
        "Type": "Typ",
        "Add": "Přidat",
        "Inputs": "Vstupy",
        "Parameters": "Parametry",
        "Outputs": "Výstupy",
        "Add output": "Přidat výstup",
        "Add parameter": "Přidat parametr",
        "Add input": "Přidat vstup",
        "Default value": "Výchozí hodnota",
        "Variable name": "Jméno proměnné",
        "New value": "Nová hodnota",
        "Add value": "Přidat hodnotu",
        "Tasks list": "Seznam úloh",
        "Delete selected tasks": "Smazat vybrané úlohy",
        "Select function": "Vybrat funkci",
        "Select device for function": "Vybrat zařízení pro funkci",
        "copy": "kopie",
        "Mandatory only": "Jen povinné",
        "Add producer": "Přidat producenta",
        "Add processor": "Přidat zpracovatele",
        "Add consumer": "Přidat konzumenta",
        "Task settings": "Nastaveni a správa úlohy",
        "Copy task": "Kopirovat úlohu",
        "Mandatory parameters": "Povinne parametry",
        "Close": "Zavřít",
        "Task schema": "Schéma úlohy",
    },
}

var language = 'en';

module.exports.tr = function (key) {
    if (translations[language]) {
        return translations[language][key] || key;
    } else {
        return key;
    }
}

module.exports.setLanguage = function (lang) {
    language = lang;
}