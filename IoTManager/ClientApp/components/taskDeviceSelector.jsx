﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Button, Checkbox, Icon, Table, Input } from 'semantic-ui-react'
import { tr } from './translation';

var TaskDeviceSelector = createReactClass({

    getInitialState: function () {
        return { text: '', lines: 5 };
    },

    toggleShowAll() {
        this.setState({ lines: (this.state.lines < 0) ? 5:-1 });
    },

    filterByText(event, { value }) {
        this.setState({ text: value });
    },

    deviceSelected(device) {
        if (this.props.onSelected) this.props.onSelected(device);
    },

    render: function () {
        var lines = 0;
        var rows = this.props.devices.filter(function (device) {
            if (this.state.text) {
                if (!device.description && !device.group) return false;
                if (!(device.description && device.description.toLowerCase().search(this.state.text) != -1) &&
                    !(device.groupName && device.groupName.toLowerCase().search(this.state.text) != -1)) {
                    return false;
                }
            }

            lines++;
            return (this.state.lines >= 0 && lines > this.state.lines) ? false : true;

        }, this)
            .map((device, index) => {
                var active = this.props.device && device.id == this.props.device.id;
                return (
                    <Table.Row active={active} key={device.id} className="cursor-pointer" onClick={this.deviceSelected.bind(this, device)}>
                        <Table.Cell>{device.description}</Table.Cell>
                        <Table.Cell collapsing>{device.groupName}</Table.Cell>
                    </Table.Row>
                );
            });

        return (
            <div>
                <Input icon='search' value={this.state.text} onChange={this.filterByText} />
                <Table striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Popis</Table.HeaderCell>
                            <Table.HeaderCell>Skupina</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {rows}
                    </Table.Body>
                </Table>
                <div className="footer">
                    <Button onClick={this.toggleShowAll}>
                        {(this.state.lines < 0) ? tr("Show") + " 5" : tr("Show all")}
                    </Button>
                </div>
            </div>
        );
    }
});

module.exports = TaskDeviceSelector;
