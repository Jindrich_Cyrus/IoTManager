﻿var React = require('react');
var createReactClass = require('create-react-class');
import { TextArea, Select, Input, Button, Table, Form, Link } from 'semantic-ui-react'

var VersionInfo = createReactClass({

    numberChanged: function (event, { value }) {
        this.props.version.number = value;
        if (this.props.versionChanged) this.props.versionChanged(this.props.version);
    },

    stateChanged: function (event, { value }) {
        this.props.version.stateId = value;
        if (this.props.versionChanged) this.props.versionChanged(this.props.version);
    },

    descriptionChanged: function (event, { value }) {
        this.props.version.description = value;
        if (this.props.versionChanged) this.props.versionChanged(this.props.version);
    },

    fileChanged: function (event) {
        if (event.target.files && event.target.files.length > 0) {
            this.props.version.file = event.target.files[0];
            if (this.props.versionChanged) this.props.versionChanged(this.props.version);
        }
        event.target.value = null;
    },

    render: function () {

        var versionButton = null;

        if (this.props.version.stored) {
            var downloadLink = "Function.downloadVersion/?id=" + this.props.functionId + "&versionId=" + this.props.version.id;
            versionButton = <a href={downloadLink}><Button>Download</Button></a>
        } else {
            versionButton = < label className="ui button right floated" > {(this.props.version.file && this.props.version.file.name) || 'Upload'} < input type="file" onChange={this.fileChanged} /></label >
        }
        
        return (
            <Table.Row>
                <Table.Cell collapsing>
                    <Input type="text" value={this.props.version.number ? this.props.version.number : ''} onChange={this.numberChanged} />
                </Table.Cell>
                <Table.Cell collapsing>
                    <Select options={this.props.versionStates} value={this.props.version.stateId} onChange={this.stateChanged} />
                </Table.Cell>
                <Table.Cell>
                    <Input fluid type="text" value={this.props.version.description ? this.props.version.description : ''} onChange={this.descriptionChanged} />
                </Table.Cell>
                <Table.Cell collapsing>
                    {versionButton}
                </Table.Cell>
            </Table.Row>
        );
    }
});

module.exports = VersionInfo;
