﻿var React = require('react');
var createReactClass = require('create-react-class');

var InputLink = createReactClass({

    shouldComponentUpdate() {
        return true;
    },

    render: function () {

        var dataSourceId = "output_" + this.props.dataSource.hostKey + "_" + this.props.dataSource.variableKey;
        var thisId = "input_" + this.props.hostKey + "_" + this.props.variableKey;

        var dataSourceElement = document.getElementById(dataSourceId);
        var thisElement = document.getElementById(thisId);

        if (!dataSourceElement || !thisElement) return null;

        var dataSourceBox = dataSourceElement.getBoundingClientRect();
        var thisBox = thisElement.getBoundingClientRect();

        var width = thisBox.left - dataSourceBox.right;
        var height = (thisBox.top + (thisBox.height / 2)) - (dataSourceBox.top + (dataSourceBox.height / 2));

        var boxHeight = Math.abs(height);
        if (boxHeight < 3) boxHeight = 3;

        var top = Math.min(thisBox.top + (thisBox.height / 2), dataSourceBox.top + (dataSourceBox.height / 2)) + window.pageYOffset;

        var scaleY = 1;
        var translateY = 0;
        if (height < 0) {
            scaleY = - 1;
            translateY = height;
        }

        var initialDirectionSize = 40;
        var minWidth = 20;
        if (width >= 0) {
            var boxWidth = width;
            if (boxWidth < minWidth) boxWidth = minWidth;
            var translateX = (width < minWidth) ? (minWidth - width) / 2 : 0;
            var pathCommand = "M0,0 c " + initialDirectionSize + " 0, " + (boxWidth - initialDirectionSize) + " " + boxHeight + ", " + boxWidth + " " + boxHeight;
            var left = dataSourceBox.right + window.pageXOffset - translateX;
        } else {
            initialDirectionSize = 10;
            var boxWidth = -width + minWidth;
            var translateX = minWidth / 2;
            //var pathCommand = "M" + (-width) + ",0 c " + initialDirectionSize + " 0, " + (width - initialDirectionSize) + " " + boxHeight + ", " + width + " " + boxHeight;
            var backLineY = (thisBox.height / 2) + 2;
            var pathCommand = "M" + (-width) + ",0 c " + initialDirectionSize + " 0, " + initialDirectionSize + " " + backLineY + ", " + 0 + " " + backLineY +
                " c " + (initialDirectionSize) + " 0, " + (-initialDirectionSize) + " " + 0 + ", " + width + " " + 0 +
                " c " + (-initialDirectionSize) + " 0, " + (-initialDirectionSize) + " " + (height - backLineY) + ", " + 0 + " " + (height - backLineY);
            var left = thisBox.left + window.pageXOffset - translateX;
        }

        var svgStyle = { "position": "absolute", "top": top, "left": left };
        var transform = "scale(1, " + scaleY + ") translate(" + translateX + "," + translateY + ")"

        //< line x1= "0" y1= "0" x2= { width - 10 } y2= { height - 10 } stroke= "red" transform= { transform } />

        var className = "link connected " + this.props.className;
        return (
            <svg className={className} height={boxHeight} width={boxWidth} style={svgStyle}>
                <path d={pathCommand} fill="transparent" transform={transform} />
            </svg>
        );
    }
});

module.exports = InputLink;
