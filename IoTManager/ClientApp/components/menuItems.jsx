﻿var React = require('react');
var createReactClass = require('create-react-class');

var MenuItems = createReactClass({

    render: function () {
        return (
            <div className="filter-content">
                <div className="box active" onClick={() => this.props.selectedChanged(1)}>Funkcni IoT</div>
                <div className="box update" onClick={() => this.props.selectedChanged(2)}>Aktualizace</div>
                <div className="box inactive" onClick={() => this.props.selectedChanged(0)}>Nefunkcni IoT</div>
                <div className="box all" onClick={() => this.props.selectedChanged(-1)}>Vše dle pořadí</div>
            </div>
        );
    }
});

module.exports = MenuItems;