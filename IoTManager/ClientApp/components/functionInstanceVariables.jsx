﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Segment, Form, Menu, Button, Input, Icon, Checkbox } from 'semantic-ui-react'
var FunctionVariableSetter = require('./functionVariableSetter');
import { getName, isEqualVariableTypes } from './utils'

var FunctionInstanceVariables = createReactClass({

    getInitialState: function () {
        return { showParameters: false };
    },

    componentDidMount: function () {
        if (this.props.refreshLinks) this.props.refreshLinks();
    },

    toggleParameters: function () {
        this.setState({ showParameters: !this.state.showParameters });
        this.props.refreshLinks();
    },

    parameterChanged: function () {
        if (this.props.parameterChanged) this.props.parameterChanged(this.props.function)
    },

    selectedInputChanged: function (key) {
        if (this.props.selectedInputChanged) this.props.selectedInputChanged(key);
    },

    selectedOutputChanged: function (key) {
        if (this.props.selectedOutputChanged) this.props.selectedOutputChanged(key);
    },

    unlink: function (key) {
        if (this.props.unlink) this.props.unlink(key);
    },

    render: function () {

        var parameters = this.props.function.parameters ?
            Object.keys(this.props.function.parameters).map((key, index) =>
                <FunctionVariableSetter key={key} id={key} variable={this.props.function.parameters[key]} valueChanged={this.parameterChanged} />
            ) : null;

        var inputs = this.props.function.inputs ?
            Object.keys(this.props.function.inputs).map((key, index) => {
                var className = "";
                var color = null;
                var basic = true;
                if (this.props.selectedOutput && isEqualVariableTypes(this.props.selectedOutput.variable, this.props.function.inputs[key]) && this.props.hostKey != this.props.selectedOutput.hostKey) {
                    className += ' selected';
                    color = "orange";
                }
                if (this.props.selectedInput && this.props.selectedInput.variableKey == key && this.props.hostKey == this.props.selectedInput.hostKey) {
                    className += ' selected';
                    color = "orange";
                    basic = false;
                }
                var id = "input_" + this.props.hostKey + "_" + key;
                return (
                    <div id={id} key={key} className="box horizontal center">
                        <div className="variable">
                            <Button icon labelPosition='left' basic={basic} color={color} className={className} onClick={!this.props.function.inputs[key].dataSource && this.selectedInputChanged.bind(this, key)}>
                                <Icon color={color} onClick={this.props.function.inputs[key].dataSource && this.unlink.bind(this, key)}>
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000">                                        <g><g><path d="M322.8,500c0,28.8,23.4,52.1,52.1,52.1S427,528.8,427,500s-23.4-52.1-52.1-52.1S322.8,471.2,322.8,500z M573,500c0,28.8,23.4,52.1,52.1,52.1s52.1-23.4,52.1-52.1s-23.4-52.1-52.1-52.1S573,471.2,573,500z M10,906.6c0,46.1,37.3,83.4,83.4,83.4h813.2c46.1,0,83.4-37.3,83.4-83.4l0-813.2c0-46.1-37.3-83.4-83.4-83.4L93.4,10C47.3,10,10,47.3,10,93.4V906.6L10,906.6z M218.5,500c0-155.5,125.9-281.5,281.5-281.5S781.5,344.5,781.5,500S655.5,781.5,500,781.5S218.5,655.5,218.5,500z" /></g></g>                                    </svg>
                                </Icon>
                                {key}
                            </Button>
                        </div>
                    </div>
                );
            }
            ) : null;

        var outputs = this.props.function.outputs ?
            Object.keys(this.props.function.outputs).map((key, index) => {
                var className = "text-alight-right";
                var color = null;
                var basic = true;
                if (this.props.selectedInput && isEqualVariableTypes(this.props.selectedInput.variable, this.props.function.outputs[key]) && this.props.hostKey != this.props.selectedInput.hostKey) {
                    className += ' selected';
                    color = "orange";
                }
                if (this.props.selectedOutput && this.props.selectedOutput.variableKey == key && this.props.hostKey == this.props.selectedOutput.hostKey) {
                    className += ' selected';
                    color = "orange";
                    basic = false;
                }
                var id = "output_" + this.props.hostKey + "_" + key;
                return (
                    <div id={id} key={key} className="variable">
                        <Button icon labelPosition='right' basic={basic} color={color} onClick={this.selectedOutputChanged.bind(this, key)} className={className}>
                            {key}
                            <Icon color={color}>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000">                                    <g><g transform="rotate(90 500,500) translate(0.000000,512.000000) scale(0.100000,-0.100000)"><path d="M3043.8,3551.9l-5.7-1470l-983.8-5.7l-981.9-3.8l9.6-685.2c9.6-723.5,24.9-876.6,118.7-1249.9c239.3-939.8,815.4-1755.2,1627-2296.9c245-164.6,656.5-363.7,947.5-457.5l248.8-82.3v-1039.3V-4780H5000h976.2v1041.3v1039.3l245,80.4c581.9,189.5,1108.3,511,1544.7,949.4c604.8,602.9,974.3,1332.2,1112.1,2191.6c23,149.3,34.5,375.2,40.2,874.7l9.6,675.7l-981.9,3.8l-983.8,5.7l-5.7,1470l-3.8,1468.1h-488.1h-488.1V3546.2V2072.3H5000h-976.2v1473.8V5020h-488.1h-488.1L3043.8,3551.9z" /></g></g>                                </svg>
                            </Icon>
                        </Button>
                    </div>
                )
            }
            ) : null;

        return (
            <div className="box">
                <Form>
                    <Form.Input icon="pencil" label='Funkce' value={this.props.function && getName(this.props.function.description)} onClick={this.props.handleEditFunction} />
                </Form>

                <Form>
                    <div className="field">
                        <label>Vstupy</label>
                        {inputs && Object.keys(inputs).length > 0 && inputs}
                    </div>
                </Form>

                <Form>
                    <div className="field">
                        <label>Parametry {<Checkbox toggle className="green" checked={this.state.showParameters} onChange={this.toggleParameters} label='Jen povinné' />}</label>
                        {parameters && parameters.length > 0 && this.state.showParameters && parameters}
                        </div>
                </Form>

                <Form>
                    <div className="field">
                        <label>Výstupy</label>
                    {outputs && Object.keys(outputs).length > 0 && outputs}
                    </div>
                </Form>
            </div>
        );
    }
});


module.exports = FunctionInstanceVariables;
