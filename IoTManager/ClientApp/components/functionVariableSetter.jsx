﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Segment, TextArea, Select, Input, Form, Button} from 'semantic-ui-react'
var validator = require('validator');

var IntegerFunctionVariable = createReactClass({

    valueChanged: function (event, { value }) {
        //e.preventDefault();
        this.props.variable.value = value;
        if (this.props.valueChanged) this.props.valueChanged(this.props.variable);
    },

    render: function () {
        var valid = validator.isInt(String(this.props.variable.value), { locale: globalLanguageCountry });
        return (
            <Form>
                <Form.Input error={!valid} value={this.props.variable.value || ''} label={this.props.variable.description} onChange={this.valueChanged} />
            </Form>
        );
    }
});

var FloatFunctionVariable = createReactClass({

    valueChanged: function (event, { value }) {
        //e.preventDefault();
        this.props.variable.value = value;
        if (this.props.valueChanged) this.props.valueChanged(this.props.variable);
    },

    render: function () {
        var valid = validator.isFloat(String(this.props.variable.value), { locale: globalLanguageCountry });

        return (
            <Form>
                <Form.Input fluid error={!valid} value={this.props.variable.value || ''} label={this.props.variable.description} onChange={this.valueChanged} />
            </Form>
        );
    }
});

var StringFunctionVariable = createReactClass({

    valueChanged: function (event, { value }) {
        //e.preventDefault();
        this.props.variable.value = value;
        if (this.props.valueChanged) this.props.valueChanged(this.props.variable);
    },

    render: function () {
        return (
            <Form>
                <Form.Input value={this.props.variable.value || ''} label={this.props.variable.description} onChange={this.valueChanged} />
            </Form>
        );
    }
});

var BooleanFunctionVariable = createReactClass({

    valueChanged: function (event, object) {
        //e.preventDefault();
        this.props.variable.value = object.checked;
        if (this.props.valueChanged) this.props.valueChanged(this.props.variable);
    },

    render: function () {
        return (
            <Form>
                <Form.Checkbox toggle checked={this.props.variable.value} label={this.props.variable.description} onChange={this.valueChanged} />
            </Form>
        );
    }
});

var EnumFunctionVariable = createReactClass({

    valueChanged: function (event, { value }) {
        //e.preventDefault();
        this.props.variable.value = value;
        if (this.props.valueChanged) this.props.valueChanged(variable);
    },

    render: function () {

        var enumValues = this.props.variable.enumValues ? this.props.variable.enumValues.map((value, index) => { return { key: index, value: value, text: value }; }) : null;

        var valid = enumValues && enumValues.indexOf(String(stringValue)) != -1;

        return (
            <Form>
                <Form.Select error={!valid} options={enumValues} value={this.props.variable.value || ''} label={this.props.variable.description} onChange={this.valueChanged} />
            </Form>
        );
    }
});

var FunctionVariableSetter = createReactClass({

    valueChanged: function (value) {
        if (this.props.valueChanged) this.props.valueChanged(this.props.id, value);
    },

    render: function () {

        // Boolean, Integer, Float, String, Enum
        switch (this.props.variable.type) {
            case 0: return <BooleanFunctionVariable variable={this.props.variable} valueChanged={this.valueChanged} />;
            case 1: return <IntegerFunctionVariable variable={this.props.variable} valueChanged={this.valueChanged} />;
            case 2: return <FloatFunctionVariable variable={this.props.variable} valueChanged={this.valueChanged} />;
            case 3: return <StringFunctionVariable variable={this.props.variable} valueChanged={this.valueChanged} />;
            case 4: return <EnumFunctionVariable variable={this.props.variable} valueChanged={this.valueChanged} />;
        }

        return null;
    }
});

module.exports = FunctionVariableSetter;
