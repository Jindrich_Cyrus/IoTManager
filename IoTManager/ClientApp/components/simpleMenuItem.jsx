﻿var React = require('react');
var createReactClass = require('create-react-class');

var SimpleMenuItem = createReactClass({
    render: function () {
        let className = "box " + this.props.class;
        return (
            <div className={className} onClick={this.props.click}>{this.props.text}</div>
        );
    }
});

module.exports = SimpleMenuItem;