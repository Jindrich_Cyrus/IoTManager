﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Button, Checkbox, Icon, Table } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { tr } from './translation';

var FunctionRow = createReactClass({

    handleCheckedChange: function (e) {
        //e.preventDefault();
        this.props.checkedChanged(this.props.function.id);
    },

    hasProperties(object) {
        if (!object) return false;
        return Object.keys(object).length > 0;
    },

    render: function () {
        var buttonClass = "";
        if (this.hasProperties(this.props.function.inputs) && this.hasProperties(this.props.function.outputs)) {
            buttonClass = "yellow";
        } else {
            if (this.hasProperties(this.props.function.inputs)) {
                buttonClass = "red";
            }
            else if (this.hasProperties(this.props.function.outputs)) {
                buttonClass = "green";
            }
        }
        return (
            <Table.Body>
                <Table.Row>
                    <Table.Cell collapsing>
                        <Checkbox checked={this.props.checked} onChange={this.handleCheckedChange} />
                    </Table.Cell>
                    <Table.Cell>
                        <NavLink to={{ pathname: '/function/' + this.props.function.id }} >
                        {this.props.function.name}
                        {this.props.function.description &&
                            <span className="description"> {this.props.function.description} </span>
                        }
                        </NavLink>
                    </Table.Cell>
                    <Table.Cell collapsing><NavLink to={{ pathname: '/function/' + this.props.function.id }} ><Button basic className={buttonClass} >{tr("Detail")}</Button></NavLink></Table.Cell>
                </Table.Row>
            </Table.Body>
        );
    }
});

module.exports = FunctionRow;
