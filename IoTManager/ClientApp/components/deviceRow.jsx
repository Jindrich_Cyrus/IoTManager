﻿var React = require('react');
var createReactClass = require('create-react-class');
import { Button, Checkbox, Icon, Table } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import { tr } from './translation';

var DeviceRow = createReactClass({

    handleCheckedChange: function (e) {
        //e.preventDefault();
        if (this.props.checkedChanged) this.props.checkedChanged(this.props.device.id);
    },

    render: function () {
        var buttonClass = this.props.device.online ? "green" : "red";
        return (
            <Table.Body>
                <Table.Row>
                    <Table.Cell collapsing><Checkbox checked={this.props.checked} onChange={this.handleCheckedChange} /></Table.Cell>
                    <Table.Cell><NavLink to={{ pathname: '/device/' + this.props.device.id }} >{this.props.device.description}</NavLink></Table.Cell>
                    <Table.Cell collapsing>{this.props.device.groupName}</Table.Cell>
                    <Table.Cell collapsing>{this.props.device.updates && <i className="fa fa-refresh update" aria-hidden="true"></i>}</Table.Cell>
                    <Table.Cell collapsing><NavLink to={{ pathname: '/device/' + this.props.device.id }} ><Button basic className={buttonClass}>{tr("Detail")}</Button></NavLink></Table.Cell>
                </Table.Row>
            </Table.Body>
        );
    }
});

module.exports = DeviceRow;
