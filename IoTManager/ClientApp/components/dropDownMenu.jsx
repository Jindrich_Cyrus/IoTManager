﻿var React = require('react');
var createReactClass = require('create-react-class');

var DropDownMenu = createReactClass({

    getInitialState: function () {
        return { isDown: false };
    },

    toggleDownHandler() {
        this.setState({ isDown: !this.state.isDown });
    },

    render: function () {
        var stateClass = "menu-header box " + this.props.class;
        return (
            <div>
                <div className={stateClass} onClick={this.toggleDownHandler}>
                    <div className="content">Filtrovat</div>
                </div>
                {this.state.isDown &&
                    <div className="menu-content">
                    {this.props.children}
                    </div>
                }
            </div>
        );
    }
});

module.exports = DropDownMenu;