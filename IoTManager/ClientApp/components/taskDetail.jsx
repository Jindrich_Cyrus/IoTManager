﻿var React = require('react');
var createReactClass = require('create-react-class');
var FunctionVariable = require('./functionVariable');

var FunctionDetail = createReactClass({

    getInitialState: function () {
        return { parameters: {}, changed: false };
    },

    componentDidMount: function () {
        if (this.props.task) {
            this.setState({ parameters: this.props.task.parameters })
        }
    },

    propertyChanged: function (key, value) {
        var parameters = this.state.parameters;
        var parameter = parameters[key];
        parameter.value = value;
        this.setState({ parameters: parameters, changed: true });


        //if (this.props.valueChanged) this.props.propertyChanged(key, value);
    },

    updateFunction() {
        var formData = new FormData();
        formData.append('id', this.props.task.editId);
        formData.append('versionId', 45);
        formData.append('parameters', JSON.stringify(this.state.parameters));

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "device/UpdateFunction");
        xhr.onload = function () {
            // this.parseResponse(xhr.responseText);
        }.bind(this);
        xhr.onerror = function () {
        }.bind(this);
        xhr.ontimeout = function () {
        }.bind(this);
        xhr.send(formData);
    },

    render: function () {

        //var parameters = Object.entries(this.props.task.parameters).map((parameter, index) =>
        //    <FunctionVariable key={parameter[0]} data={parameter[1]} valueChanged={this.valueChanged} />
        //);

        var parameters = Object.entries(this.state.parameters).map((parameter, index) =>
            <FunctionVariable key={parameter[0]} id={parameter[0]} data={parameter[1]} propertyChanged={this.propertyChanged} />
        );

        return (
            <div className="function-detail">
                    {parameters}
                    {this.state.changed &&
                        <button onClick={this.updateFunction}>Změnit</button>
                    }
            </div>
        );
    }
});

var TaskDetail = createReactClass({

    render: function () {
        return (
            <div className="task-detail">
                <FunctionDetail data={this.props.task.ifFunction} />
                <FunctionDetail data={this.props.task.thenFunction} />
            </div>
        );
    }
});

module.exports = TaskDetail;
