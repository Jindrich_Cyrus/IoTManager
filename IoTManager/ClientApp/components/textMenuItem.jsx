﻿var React = require('react');
var createReactClass = require('create-react-class');

var TextMenuItem = createReactClass({

    valueChange: function (e) {
        //e.preventDefault();
        if (this.props.valueChange) this.props.valueChange(e.target.value);
    },

    render: function () {
        let className = "box " + this.props.class;
        return (
            <div className={className}><input type="text" onChange={this.valueChange} /></div>
        );
    }
});

module.exports = TextMenuItem;