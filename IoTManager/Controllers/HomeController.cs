using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using IoTManager.Models;
using Microsoft.Extensions.Options;
using NLog.Web;
using Microsoft.Extensions.Logging;

namespace IoTManager.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IStringLocalizer<HomeController> localizer;

        private readonly ILogger<HomeController> logger;

        public HomeController(IStringLocalizer<HomeController> localizer, IHostingEnvironment env, IOptions<AppSettings> appSettings, ILogger<HomeController> logger) : base(env, appSettings, logger) {
            this.localizer = localizer;
            this.logger = logger;
        }

        public IActionResult Index(string culture)
        {
            logger.LogInformation("Index page");

            switch (culture)
            {
                case "en-GB":
                case "cs-CZ":
                    var cultureInfo = new RequestCulture(culture);
                    Response.Cookies.Append(
                        CookieRequestCultureProvider.DefaultCookieName,
                        CookieRequestCultureProvider.MakeCookieValue(cultureInfo),
                        new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
                    );
                    break;
            }

            var userId = GetUserId();
            if (!userId.HasValue)
            {
                return RedirectToAction("Login", "Home");
            }

            var UserData = UserViewData.FromDatabase(userId.Value, appSettings.Value.ConnectionString);
            ViewData.Model = JsonConvert.SerializeObject(UserData);

            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            logger.LogInformation("Login page");
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel user)
        {
            logger.LogInformation("Login page post");
            if (!string.IsNullOrWhiteSpace(user.UserName))
            {
                var userId = user.IsValid(user.UserName, user.Password, appSettings.Value.ConnectionString);
                if (userId.HasValue)
                {
                    HttpContext.Session.Set("UserId", BitConverter.GetBytes(userId.Value));
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }
            }
            else
            {
                ModelState.AddModelError("", "Login data is incorrect!");
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Remove("UserId");

            return RedirectToAction("Login", "Home");
        }
    }
}
