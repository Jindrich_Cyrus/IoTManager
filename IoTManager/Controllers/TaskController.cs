﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using IoTManager.Models;
using NLog;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Npgsql;
using System.Transactions;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace IoTManager.Controllers
{
    public class TaskController : BaseController
    {
        private readonly ILogger<TaskController> logger;

        public TaskController(IHostingEnvironment env, IOptions<AppSettings> appSettings, ILogger<TaskController> logger) : base(env, appSettings, logger) {
            this.logger = logger;
        }

        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult List()
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            return Json(GetTasks(userId.Value));
        }

        string TasksTableDeviceGroupContraint = @"((NOT EXISTS(SELECT device_id FROM task_host WHERE task_host.task_id = tasks.task_id)) OR(EXISTS(SELECT task_host.device_id FROM task_host, devices WHERE task_host.task_id = tasks.task_id AND task_host.device_id = devices.device_id AND(devices.group_id IS NULL OR devices.group_id IN(SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id)))))";
        string NonTasksTableDeviceGroupContraint = @"((NOT EXISTS(SELECT device_id FROM task_host WHERE task_host.task_id = @task_id)) OR(EXISTS(SELECT task_host.device_id FROM task_host, devices WHERE task_host.task_id = @task_id AND task_host.device_id = devices.device_id AND(devices.group_id IS NULL OR devices.group_id IN(SELECT user_group.group_id FROM user_group WHERE user_group.user_id = @user_id)))))";
        private List<TaskModel> GetTasks(long userId)
        {
            var tasks = new List<TaskModel>();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand command = new NpgsqlCommand("SELECT tasks.task_id, tasks.description, tasks.name, tasks.active FROM tasks WHERE(NOT tasks.deleted) AND " + TasksTableDeviceGroupContraint + " ORDER BY tasks.task_id", connection);
                    command.Parameters.AddWithValue("@user_id", userId);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tasks.Add(new TaskModel()
                            {
                                Id = Convert.ToInt64(reader["task_id"]),
                                Description = reader["description"] as string,
                                Name = reader["name"] as string,
                                Active = reader["active"] as bool?,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read tasks");
            }

            return tasks;
        }

        public IActionResult Detail(int id)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            return Json(GetTask(id, userId.Value));
        }

        private TaskModel GetTask(long id, long userId)
        {
            TaskModel task = null;
            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();
                    {
                        {
                            NpgsqlCommand command = new NpgsqlCommand("SELECT tasks.task_id, tasks.description, tasks.name, tasks.active, tasks.hosts FROM tasks WHERE tasks.task_id = @task_id AND (NOT tasks.deleted) AND " + TasksTableDeviceGroupContraint, connection);
                            command.Parameters.AddWithValue("@task_id", id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    task = new TaskModel()
                                    {
                                        Stored = true,
                                        Id = Convert.ToInt64(reader["task_id"]),
                                        Description = reader["description"] as string,
                                        Name = reader["name"] as string,
                                        Active = reader["active"] as bool?,
                                        Hosts = (reader["hosts"] != DBNull.Value) ? JsonConvert.DeserializeObject<Dictionary<int, HostedFunction>>(reader["hosts"] as string) : new Dictionary<int, HostedFunction>(),
                                    };
                                }
                            }
                        }
                        // get task functions to merge
                        if (task != null)
                        {
                            var functions = new Dictionary<long, FunctionModel>();
                            NpgsqlCommand command = new NpgsqlCommand("SELECT functions.function_id, functions.description, functions.local, functions.parameters, functions.inputs, functions.outputs FROM functions WHERE functions.function_id IN(SELECT DISTINCT task_host.function_id FROM task_host WHERE task_host.task_id = @task_id)", connection);
                            command.Parameters.AddWithValue("@task_id", id);
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    var function = new FunctionModel()
                                    {
                                        Id = Convert.ToInt64(reader["function_id"]),
                                        Description = reader["description"] as string,
                                        Local = reader["local"] as bool?,
                                    };

                                    function.Parameters = ((reader["parameters"] as string) != null) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["parameters"] as string) : null;
                                    function.Inputs = ((reader["inputs"] as string) != null) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["inputs"] as string) : null;
                                    function.Outputs = ((reader["outputs"] as string) != null) ? JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(reader["outputs"] as string) : null;

                                    functions.Add(function.Id, function);
                                }
                            }
                            // merge functions
                            if (task.Hosts != null)
                            {
                                foreach (var host in task.Hosts.Values)
                                {
                                    if (host.Function != null)
                                    {
                                        var latestFunction = functions[host.Function.Id];

                                        host.Function.Parameters = UpdateFunctionVariables(JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(JsonConvert.SerializeObject(latestFunction.Parameters)), host.Function.Parameters);
                                        host.Function.Inputs = UpdateFunctionVariables(JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(JsonConvert.SerializeObject(latestFunction.Inputs)), host.Function.Inputs);
                                        host.Function.Outputs = UpdateFunctionVariables(JsonConvert.DeserializeObject<Dictionary<string, FunctionVariable>>(JsonConvert.SerializeObject(latestFunction.Outputs)), host.Function.Outputs);

                                        // check links
                                        if (host.Function.Inputs != null)
                                        {
                                            foreach (var input in host.Function.Inputs)
                                            {
                                                if (input.Value.DataSource != null)
                                                {
                                                    if (!(task.Hosts.ContainsKey(input.Value.DataSource.HostKey) &&
                                                        task.Hosts[input.Value.DataSource.HostKey].Function != null &&
                                                        task.Hosts[input.Value.DataSource.HostKey].Function.Outputs != null &&
                                                        task.Hosts[input.Value.DataSource.HostKey].Function.Outputs.ContainsKey(input.Value.DataSource.VariableKey) &&
                                                        task.Hosts[input.Value.DataSource.HostKey].Function.Outputs[input.Value.DataSource.VariableKey].Type == input.Value.Type))
                                                    {
                                                        input.Value.DataSource = null;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read task");
                return new TaskModel() { Error = ex.Message };
            }

            return task;
        }

        private static Dictionary<string, FunctionVariable> UpdateFunctionVariables(Dictionary<string, FunctionVariable> latestVariables, Dictionary<string, FunctionVariable> originalVariables)
        {
            // update existing variables
            if (originalVariables != null && latestVariables != null)
            {
                foreach (var latestVariable in latestVariables)
                {
                    if (originalVariables.ContainsKey(latestVariable.Key))
                    {
                        var originalVariable = originalVariables[latestVariable.Key];
                        if (originalVariable.Type == latestVariable.Value.Type) latestVariable.Value.Value = originalVariables[latestVariable.Key].Value;
                        latestVariable.Value.DataSource = originalVariable.DataSource;
                        latestVariable.Value.Stored = originalVariable.Stored;
                    }
                }
            }
            return latestVariables;
        }

        public IActionResult Active(int id, bool active)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            try
            {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                {
                    connection.Open();

                    NpgsqlCommand command = new NpgsqlCommand("UPDATE tasks SET active = @active, modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE tasks.task_id = @task_id AND " + TasksTableDeviceGroupContraint, connection);
                    command.Parameters.AddWithValue("@task_id", id);
                    command.Parameters.AddWithValue("@user_id", userId);
                    command.Parameters.AddWithValue("@active", active);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read devices");
                return Json(new { error = ex.Message });
            }

            //TODO vyresit publish vsem zarizenim s moznosti anulovat funkce pro !tasks.active 
            //foreach (var device_id in listOfDevices) {
            //    Publish($"manager/device/{device_id}", "updated functions");
            //}
            return Json(GetTasks(userId.Value));
        }

        public IActionResult Delete(int id)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                    {
                        connection.Open();
                        {
                            NpgsqlCommand command = new NpgsqlCommand("UPDATE tasks SET deleted = TRUE, modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE tasks.task_id = @task_id AND (NOT tasks.deleted) AND " + TasksTableDeviceGroupContraint, connection);
                            command.Parameters.AddWithValue("@task_id", id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("UPDATE task_host SET modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE task_id = @task_id AND " + NonTasksTableDeviceGroupContraint, connection);
                            command.Parameters.AddWithValue("@task_id", id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM task_host WHERE task_id = @task_id AND " + NonTasksTableDeviceGroupContraint, connection);
                            command.Parameters.AddWithValue("@task_id", id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.ExecuteNonQuery();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to read devices");
                return Json(new { error = ex.Message });
            }

            return Json(GetTasks(userId.Value));
        }

        [HttpPost]
        public IActionResult Edit([FromBody]TaskModel task)
        {
            var userId = GetUserId();
            if (!userId.HasValue) return Unauthorized();
            var listOfDevices = new List<long>();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString))
                    {
                        connection.Open();

                        {
                            NpgsqlCommand command = null;
                            if (task.Stored)
                            {
                                command = new NpgsqlCommand("UPDATE tasks SET description = @description, name = @name, active = @active, hosts = @hosts, version = version + 1, modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE tasks.task_id = @task_id AND (NOT tasks.deleted) AND " + TasksTableDeviceGroupContraint, connection);
                                command.Parameters.AddWithValue("@task_id", task.Id);
                            }
                            else
                            {
                                command = new NpgsqlCommand("INSERT INTO tasks (description, name, active, hosts, modified_by) VALUES (@description, @name, @active, @hosts, (SELECT users.username FROM users WHERE users.user_id = @user_id));select currval('task_task_id_seq');", connection);
                            }

                            command.Parameters.AddWithValue("@user_id", userId);
                            if (task.Description != null) command.Parameters.AddWithValue("@description", task.Description);
                            else command.Parameters.AddWithValue("@description", DBNull.Value);
                            if (task.Name != null) command.Parameters.AddWithValue("@name", task.Name);
                            else command.Parameters.AddWithValue("@name", DBNull.Value);
                            command.Parameters.AddWithValue("@active", task.Active ?? false);
                            if (task.Hosts != null && task.Hosts.Any()) command.Parameters.AddWithValue("@hosts", JsonConvert.SerializeObject(task.Hosts));
                            else command.Parameters.AddWithValue("@hosts", DBNull.Value);

                            if (task.Stored)
                            {
                                command.ExecuteNonQuery();
                            }
                            else
                            {
                                task.Id = Convert.ToInt64(command.ExecuteScalar());
                            }
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("UPDATE task_host SET modified_by = (SELECT users.username FROM users WHERE users.user_id = @user_id) WHERE task_id = @task_id AND " + NonTasksTableDeviceGroupContraint, connection);
                            command.Parameters.AddWithValue("@task_id", task.Id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.ExecuteNonQuery();
                        }
                        {
                            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM task_host WHERE task_id = @task_id AND " + NonTasksTableDeviceGroupContraint, connection);
                            command.Parameters.AddWithValue("@task_id", task.Id);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.ExecuteNonQuery();
                        }
                        foreach (var host in task.Hosts)
                        {
                            NpgsqlCommand command = new NpgsqlCommand("INSERT INTO task_host (task_id, host_id, device_id, function_id, modified_by) VALUES (@task_id, @host_id, @device_id, @function_id, (SELECT users.username FROM users WHERE users.user_id = @user_id))", connection);
                            command.Parameters.AddWithValue("@user_id", userId);
                            command.Parameters.AddWithValue("@task_id", task.Id);
                            command.Parameters.AddWithValue("@host_id", host.Key);
                            if (host.Value.Device != null) {
                                command.Parameters.AddWithValue("@device_id", host.Value.Device.Id);
                                listOfDevices.Add(host.Value.Device.Id);
                            } else command.Parameters.AddWithValue("@device_id", DBNull.Value);
                            command.Parameters.AddWithValue("@function_id", host.Value.Function.Id);
                            command.ExecuteNonQuery();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Failed to edit task");
                return Json(new { error = ex.Message });
            }
            foreach (var device_id in listOfDevices) {
                Publish($"manager/device/{device_id}", "updated functions");                
            }
            return Json(GetTask(task.Id, userId.Value));
        }
    }
}