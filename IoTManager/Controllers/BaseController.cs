﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using IoTManager.Models;
using NLog;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Options;
using Npgsql;
using System.Text;

namespace IoTManager.Controllers
{
    public class BaseController : Controller {
        private readonly ILogger<BaseController> logger;

        protected static object DevicesFileLock = new object();
        protected static object FunctionsFileLock = new object();
        protected static object TasksFileLock = new object();

        protected string DevicesFile { get; private set; }
        protected string FunctionsFile { get; private set; }
        protected string TasksFile { get; private set; }

        public UserViewData UserData { get; private set; }

        protected string dataPath;

        protected IOptions<AppSettings> appSettings;
        protected IHostingEnvironment env;


        public const string developerRight = "developer";
        public const string administratorRight = "administrator";

        public BaseController(IHostingEnvironment env, IOptions<AppSettings> appSettings, ILogger<BaseController> logger) {
            this.logger = logger;
            this.appSettings = appSettings;
            this.env = env;

            dataPath = System.IO.Path.Combine(env.ContentRootPath, "Data");
            DevicesFile = System.IO.Path.Combine(dataPath, "devices.json");
            FunctionsFile = System.IO.Path.Combine(dataPath, "functions.json");
            TasksFile = System.IO.Path.Combine(dataPath, "tasks.json");
        }

        protected long? GetUserId() {
            byte[] UserIdBytes;
            if (HttpContext.Session.TryGetValue("UserId", out UserIdBytes)) {
                return BitConverter.ToInt64(UserIdBytes, 0);
            }
            return null;
        }

        protected bool hasRight(long userId, string right) {
            try {
                using (var connection = new NpgsqlConnection(appSettings.Value.ConnectionString)) {
                    connection.Open();
                    {
                        NpgsqlCommand command = new NpgsqlCommand("SELECT rights.name FROM user_right, rights WHERE user_right.rights_id = rights.right_id AND user_right.user_id = @user_id", connection);
                        command.Parameters.AddWithValue("@user_id", userId);
                        using (var reader = command.ExecuteReader()) {
                            while (reader.Read()) {
                                // Rights[reader["name"] as string] = true;
                                if ((reader["name"] as string) == right) return true;
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.LogError(ex, "Failed to read user rights");
            }

            return false;
        }

        public ushort Publish(string topic, string message) {
            //TODO next phase
            return (ushort)0;            
        }
    }
}